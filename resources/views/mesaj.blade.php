
@foreach($mesajlar as $ms)
<div class="item">
	@if($ms->from_id==Auth::user()->id)
	<img src="img/avatar2.png" alt="user image" class="online"/>
	<p class="message">
		<a href="#" class="name">
			<small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $ms->created_at }}</small>
			{{ $ms->user->name }}
		</a>
		{{ $ms->mesaj }}
	</p>
	@else
	<img src="img/avatar3.png" alt="user image" class="online"/>
	<p class="message">           
		<a href="#" class="name">
			<small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $ms->created_at }}</small>
			{{ $ms->mt->name }}
		</a>                                            
		{{ $ms->mesaj }}
	</p>
	@endif

</div>
@endforeach
{{ $mesajlar->links() }}