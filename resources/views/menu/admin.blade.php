<ul class="sidebar-menu">
    <li class="active">
        <a href="/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li>
        <a href="/admin/company">
            <i class="fa fa-user"></i> <span>Firmalar</span>
        </a>
    </li>
    <li>
        <a href="/admin/users">
            <i class="fa fa-user"></i> <span>Kullanıcılar</span>
        </a>
    </li>
    <li>
        <a href="/admin/segment">
            <i class="fa fa-th"></i> <span>Segment Ayarları</span>
        </a>
    </li>
    <li>
        <a href="/admin/stok">
            <i class="fa fa-th"></i> <span>Stok Yönetimi</span>
        </a>
    </li>
    <li>
        <a href="/admin/kampanya">
            <i class="fa fa-th"></i> <span>Kampanya Tanımlamaları</span>
        </a>
    </li>
    <li>
        <a href="/admin/siparis">
            <i class="fa fa-th"></i> <span>Onaylanmamış Siparişler</span>
        </a>
    </li>
</ul>