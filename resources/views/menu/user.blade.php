<ul class="sidebar-menu">
    <li class="active">
    <a href="/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li>
        <a href="/urunler">
            <i class="fa fa-th"></i> <span>Ürünler</span>
        </a>
    </li>
    <li>
        <a href="/siparis">
            <i class="fa fa-th"></i> <span>Siparişler</span>
        </a>
    </li>
    <li>
        <a href="/odeme">
            <i class="fa fa-th"></i> <span>Ödeme İşlemleri</span>
        </a>
    </li>
    <li>
        <a href="/taslaklar">
            <i class="fa fa-bar-chart-o"></i><span>Taslak Siparişler</span>
        </a>
    </li>
    <!--
    <li>
        <a href="/raporlar">
            <i class="fa fa-laptop"></i>
            <span>Raporlar</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li>
        <a href="/fiyat-liste">
            <i class="fa fa-edit"></i> <span>Fiyat Listeleri</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li> -->
    <li>
        <a href="/ayarlar">
            <i class="fa fa-table"></i> <span>Ayarlar</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
</ul>