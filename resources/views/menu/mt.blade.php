<ul class="sidebar-menu">
    <li class="active">
    <a href="/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li>
        <a href="/mt/firmalar">
            <i class="fa fa-th"></i> <span>Firmalarım</span>
        </a>
    </li>
    <li>
        <a href="/mt/siparisler">
            <i class="fa fa-th"></i> <span>Siparişler</span>
        </a>
    </li>
    <li>
        <a href="/mt/mesajlar">
            <i class="fa fa-th"></i> <span>Mesajlar</span>
        </a>
    </li>
    <li>
        <a href="/mt/ayarlar">
            <i class="fa fa-table"></i> <span>Ayarlar</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
</ul>