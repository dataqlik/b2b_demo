@extends('layouts.home')
<script type="text/javascript">
    function listeyukle(id)
    {
        $('#siparisdetay').slideDown('slow');
        $("#siparisdetay").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/mt/siparis/detay/'+id,
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#siparisdetay").html(cevap);
                }
            });
         }
     </script>
     @section('content')
     <!-- Content Header (Page header) -->
     <section class="content-header">
        <h1>
            Dashboard (Müşteri Temsilcisi)
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            6
                        </h3>
                        <p>
                            Yeni Mesaj
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-email"></i>
                    </div>
                    <a href="/mt/mesajlar" class="small-box-footer">
                        Mesaj Kutusu <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                           8
                        </h3>
                        <p>
                            Bekleyen Sipariş
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="/mt/siparisler" class="small-box-footer">
                        İncele <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                           4
                        </h3>
                        <p>
                            Firma
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="/mt/firmalar" class="small-box-footer">
                        Firma Listesi <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        
                        <p>
                            Sistemdeki Son Kurlar
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion-cash"></i>
                    </div>
                </div>
            </div><!-- ./col -->
        </div><!-- /.row -->

        <!-- top row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">

            </div><!-- /.col -->
        </div>
        <!-- /.row -->
        <section class="content-header">
            <h2>
            Bekleyen Siparişler
            </h2>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>İşlem Tarihi</th>
                                        <th>Siparis ID</th>
                                        <th>Firma</th>
                                        <th>Firma</th>
                                        <th>Kullanıcı</th>
                                        <th>Durum</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>

        </section><!-- /.content -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="width:80%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Siparis Detayı</h4>
                    </div>
                    <div class="modal-body">
                        <div id="siparisdetay"></div>
                    </div>
                </div>
            </div>
        </div>


    </section><!-- /.content -->
    @endsection

