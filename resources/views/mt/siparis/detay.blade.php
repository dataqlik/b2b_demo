<div class="collapse.in" id="collapseExample">
	<div class="well">
		<table class="table table-striped">
			<thead>
				<th>Stok Kodu</th>
				<th>Ürün Adı</th>
				<th>Sipariş Miktarı</th>
				<th>Sipariş Birimi</th>
				<th>Birim Fiyatı</th>
				<th>KDV</th>
				<th>Tutarı</th>
			</thead>
			<tbody>
				<?php $toplam = 0; $toplamTL = 0; ?>
				@foreach($sipdetay as $row)
				<tr>
					<td>{{ $row->stokkodu }}</td>
					<td>{{ $row->urun->name }}</td>
					<td>{{ $row->adet }}</td>
					<td>{{ $row->birim }}</td>
					<td>{{ $row->fiyat }} {{ $row->doviz }}</td>
					<td>% {{ round($row->urun->kdv,0) }}</td>
					<td>{{ round( ( $row->fiyat*$row->adet ) * ( 1+ ( $row->urun->kdv / 100 )  ) ,2) }} {{ $row->doviz }}</td>
				</tr>
				<?php
				$toplam = $toplam +  ($row->fiyat*$row->adet)*(1+($row->urun->kdv/100));
				$toplamTL = $toplamTL +  (($row->fiyat*$row->adet)*(1+($row->urun->kdv/100))*$row->kur);
				?>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><b>Toplam</b></td>
					<td colspan="3"><b>{{ round($toplam,2) }} {{ $row->doviz }} (KDV Dahil)</b></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><b>Toplam (TL)</b></td>
					<td colspan="3"><b>{{ round($toplamTL,2) }} TL (KDV Dahil)</b></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
