@extends('layouts.home')
@section('content')
<?php
setlocale(LC_MONETARY, 'en_US');
foreach($fdetay as $fd)
{
	$caririsk 	= round($fd->RISK_SINIRI,0);
	$borc 		= round($fd->CM_BORCT,0);
	$alacak 	= round($fd->CM_ALACT,0);
}
?>
<section class="content-header">
	<h1>
		Sipariş Detayı (işlem)
	</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-7">
			<div class="collapse.in" id="collapseExample">
				<div class="well">
					<table class="table table-striped">
						<tr>
							<td colspan="4">{{ $siparis->erpkodu }}</td>
						</tr>
						<tr>
							<td><b>Firma</b></td>
							<td>{{ $firma->cariad }}</td>
							<td><b>Siparişi Giren</b></td>
							<td>{{ $siparis->user->name }}</td>
						</tr>
						<tr>
							<td><b>Sipariş Tarihi Saati</b></td>
							<td>{{ tarihoku($siparis->created_at) }} - {{ saatoku($siparis->created_at) }}</td>
							<td><b>Firma Cari Kodu</b></td>
							<td>{{ $firma->carikod }}</td>
						</tr>

						<tr>
							<td><b>Cari Risk Limiti</b></td>
							<td>{{ number_format($caririsk,0) }} TL</td>
							<td><b>Borç / Alacak</b></td>
							<td>{{ number_format($borc-$alacak) }} TL</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			
		</div>
	</div>
	<div class="collapse.in" id="collapseExample">
		<div class="well">
			<table class="table table-striped">
				<thead>
					<th>Stok Kodu</th>
					<th>Ürün Adı</th>
					<th>Sipariş Miktarı</th>
					<th>Sipariş Birimi</th>
					<th>Birim Fiyatı</th>
					<th>Tutarı</th>
					<th>Stok Miktarı</th>
					<th>Durum</th>
					<th>Uyarı</th>
					<th></th>
				</thead>
				<tbody>
					<?php $toplam = 0; $tamam=0; ?>
					@foreach($sipdetay as $row)
					<? if($row->sipdetay->durum_id==0) { $tamam=1; } ?><tr @if($row->durum==2) style="text-decoration: line-through;" @endif>
						<td>{{ $row->stokkodu }}</td>
						<td>{{ $row->urun->name }}</td>
						<td>{{ $row->adet }}</td>
						<td>{{ $row->birim }}</td>
						<td>{{ number_format($row->fiyat,2) }} {{ $row->doviz }}</td>
						<td>{{ number_format($row->fiyat*$row->adet) }} {{ $row->doviz }}</td>
						<td>{{ round($row->urun->miktar,0,0) }}</td>
						<td><?php
							switch($row->sipdetay->durum_id)
							{
								case '1' : echo '<span class="btn btn-success btn-xs">Onaylı</span>'; break;
								case '2' : echo '<span class="btn btn-danger btn-xs">İptal</span>'; break;
								case '3' : echo '<span class="btn btn-warning btn-xs">Temin Ediliyor</span>'; break;
								default  : echo '<span class="btn btn-info btn-xs">Beklemede</span>';
							}
							?>
						</td>
						<td>
							<?php
							if($row->fiyat<-1)
							{
								echo '<a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myFiyat">Fiyat Hatalı</a>';
							}elseif($row->adet > $row->urun->miktar)
							{
								echo '<span class="btn btn-danger btn-xs">Stok Yetersiz</span>';
							}
							else
							{
								echo '<span class="btn btn-success btn-xs">Uygun</span>';
							}
							?>
						</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-success btn-xs">İşlem</button>
								<button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/mt/siparis/islem/iptal/{{ $siparis->id }}/{{ $row->id }}">İptal</a></li>
									<li><a href="/mt/siparis/islem/onay/{{ $siparis->id }}/{{ $row->id }}">Onaylı</a></li>
									<li><a href="/mt/siparis/islem/temin/{{ $siparis->id }}/{{ $row->id }}">Temin Ediliyor</a></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php
					if($row->durum<>2)
					{
						$toplam = $toplam + ($row->fiyat * $row->adet * $row->kur);
					}
					$kur = $row->kur;
					$doviz = $row->doviz;
					?>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4"> <b>{{ tarihoku($row->created_at) }}</b> tarihinde <b>{{ $doviz }}</b> kuru <b>{{ round($kur,4) }} </b> olarak alınarak hesaplanmıştır.</b></td>
						<td><b>Toplam</b></td>
						<td colspan="4"><b>{{ number_format($toplam + ($toplam * 18 / 100) , 2 ) }} TL (KDV Dahil)</b></td>
						<td></td>

					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			<div class="collapse.in" id="collapseExample">
				<div class="well">
					<form action="/mt/siparis/islem/tamam" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="siparis" id="siparis" value="{{ $siparis->erpkodu }}">
						<div class="form-group">
							<label>Özel Kod-1</label>
							<select name="kod1" id="kod1" class="form-control" required>
								<option>Kod 1 Seçiniz</option>
								<option value="D">İADE</option>
								<option value="H">İHRACAT</option>
								<option value="I">İÇ PİYASA</option>
								<option value="K">İHRACAT KAYITLI</option>
							</select>
						</div>
						<div class="form-group">
							<label>Özel Kod-2</label>
							<select name="kod2" id="kod2" class="form-control" required>
								<option>Kod 2 Seçiniz</option>
								<option value="1">Anlık Satış</option>
								<option value="2">Günlük Satış</option>
								<option value="3">Servis</option>
								<option value="4">Kargo</option>
								<option value="5">İhracat</option>
								<option value="6">Depodan Teslim</option>
								<option value="T">5/10 Tevkifat</option>
							</select>
						</div>
						<? if($tamam==0) { ?>
						<input type="submit" name="aktar" id="aktar" value="Siparişi ERP ye aktar" class="btn btn-block btn-success">
						<? } else { ?>
						<a class="btn btn-block btn-danger">Sipariş kontrolü tamamlanmadığı için işlem yapılamaz.</a>
						<? } ?>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			
		</div>
	</div>
</section>

<div class="modal fade" id="myFiyat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kurları Güncelleme</h4>
                </div>
                <div class="modal-body">
                    <div id="guncel1">
                        <div class="alert alert-warning alert-dismissable">
                            <i class="fa fa-warning"></i>
                            
                            <b>Dikkat!</b> Bu işlem sistemdeki tüm kur bilgileri güncelleyecektir.
                        </div>
                        <a class="btn btn-block btn-danger" onclick="kurguncelle();">Güncellemeyi Başlat</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection