@extends('layouts.home')
<script type="text/javascript">
    function listeyukle()
    {
        $('#detailss').slideDown('slow');
        $("#detailss").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/admin/stok/NetsisCariListe',
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#detailss").html(cevap);
                    
                }
            });
    }
</script>
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tanımlı Firmalarım
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Cari Kod</th>
                                <th>Cari Ad</th>
                                <th>Segment</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($comp as $cmp)
                            <tr>
                                <td>{{ $cmp->carikod }}</td>
                                <td>{{ $cmp->cariad }}</td>
                                <td>{{ $cmp->segment->name }}</td>
                                <td><a href="/mt/siparisler/{{ $cmp->id }}" class="btn btn-xs btn-default">Sipariş Detayları</a></td>
                            </tr>
                        @endforeach 
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section><!-- /.content -->

@endsection

