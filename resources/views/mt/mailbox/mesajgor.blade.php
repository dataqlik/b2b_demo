<div class="box box-success">

    <div class="box-footer">
        <form id="chat" method="post" action="/mt/mesajgonder">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="hidden" name="mt_id" id="mt_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="from_id" id="from_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="user_id" id="user_id" value="{{ $kim->user->id }}">
                <input class="form-control" name="mesaj" id="mesaj" required placeholder="Mesajınız..."/>
                <div class="input-group-btn">
                    <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                    <!--<a class="btn btn-success" onclick="gonder();"><i class="fa fa-plus"></i></a>-->
                </div>
            </div>
        </form>
    </div>
    <div class="box-body chat" id="chat-box">
        <div id="yazdirxx">
            @foreach($chatlog as $ms)
            <div class="item">
                @if($ms->from_id==Auth::user()->id)

                <img src="{{ asset('img/avatar3.png') }}" alt="user image" class="online"/>
                <p class="message">           
                    <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $ms->created_at }}</small>
                        {{ $ms->mt->name }}
                    </a>                                            
                    {{ $ms->mesaj }}
                </p>

                


                @else

                <img src="{{ asset('img/avatar2.png') }}" alt="user image" class="online"/>
                <p class="message">
                    <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $ms->created_at }}</small>
                        {{ $ms->user->name }}
                    </a>
                    {{ $ms->mesaj }}
                </p>

                @endif

            </div>
            @endforeach
            {{ $chatlog->links() }}
        </div>
    </div>

                        </div><!-- /.box (chat box) -->