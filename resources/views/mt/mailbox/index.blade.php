@extends('layouts.home')
<script type="text/javascript">
    function mesajac(id)
    {
        $('#mesaj').slideDown('slow');
        $("#mesaj").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/mt/mesajlar/'+id,
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#mesaj").html(cevap);
                }
            });
    }
</script>
@section('content')
<section class="content">
    <!-- MAILBOX BEGIN -->
    <div class="mailbox row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12 col-sm-12">
                            <div class="row pad">
                                <div class="col-sm-4 search-form">
                                    <form action="#" class="text-right">
                                        <div class="input-group">                                                            
                                            <input type="text" class="form-control input-sm" placeholder="Ara">
                                            <div class="input-group-btn">
                                                <button type="submit" name="q" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>                                                     
                                    </form>
                                    <table class="table table-mailbox" id="example1">
                                        @foreach($chatlog as $ch)
                                        <tr <?php if($ch->okundu==0) { echo 'class="unread"'; } ?>>
                                            <td class="name"><a onclick="mesajac({{ $ch->user_id }});">{{ $ch->user->name }}</a></td>
                                            <td class="time">{{ tarihoku($ch->created_at) }} - {{saatoku($ch->created_at) }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    {{ $chatlog->links() }}
                                </div>
                                <div class="col-sm-8">
                                    <div id="mesaj"></div>
                                </div>
                            </div><!-- /.row -->  
                        </div><!-- /.col (RIGHT) -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col (MAIN) -->
    </div>
    <!-- MAILBOX END -->

</section><!-- /.content -->


@endsection