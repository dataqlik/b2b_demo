@extends('layouts.home')
<script>
    function gonder() {
      jQuery.ajax({
      type: 'POST',//Bu kısım POST ve GET değerlerinden birini alabilir
      url: '/chat/mesajgonder',//Verinin gönderileceği sayfa

      data: $('#chat').serialize(),
      error:function(){ $('#yazdir').html("Bir hata algılandı."); }, //Hata veri
      success: function(veri) { $('#yazdir').html(veri);} //Başarılı 
  });
  }
</script>
@section('content')
<?php

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        B2B Dashboard
        <small>Anasayfa</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        {{ count($siparis) }}
                    </h3>
                    <p>
                        Adet Bekleyen Sipariş
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="/siparis" class="small-box-footer">
                    Tümünü Gör <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        
    <div class="col-lg-6 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    20.000 TL
                </h3>
                <p>
                    Toplam Borç
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-money"></i>
            </div>
            <a href="/odeme" class="small-box-footer">
                Ödeme Yap <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h5>
                            {{ $usd }}
                        </h5>
                        <h5>
                            {{ $euro }}
                        </h5>
                        <h5>
                            {{ $gbp }}
                        </h5>
                        <p>
                            Sistemdeki Son Kurlar
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion-cash"></i>
                    </div>
                </div>
            </div><!-- ./col -->
</div><!-- /.row -->


<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-6 connectedSortable"> 

            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Kampanya ve Bildirimler</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @if(count($kampanya)>0)
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                        <?php $i=0; ?>
                        <?php $ii=0; ?>                        
                        <?php foreach($kampanya as $kam) {  ?>
                            <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" <? if($i==0) { echo 'class="active"'; } ?>></li>
                            
                        <?php $i++; } ?>
                        </ol>
                        <div class="carousel-inner">
                        @foreach($kampanya as $kam2)
                            <div class="item <? if($ii==0) { echo 'active'; } ?>">
                                <img src="http://placehold.it/900x500/<? if($kam2->tip=='Bilgilendirme') { echo '39CCCC'; } else { echo 'f39c12'; } ?>/ffffff&text={{ $kam2->tip }}" alt="{{ $kam2->kampanya_ad }}">
                                
                                <div class="carousel-caption" style="color:000">
                                <h3>{{ $kam2->kampanya_ad }}</h3>
                                    <?php echo $kam2->aciklama; ?>
                                </div>
                            </div>
                            <?php $ii++; ?>
                        @endforeach
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                    @else
                        <center>Şu an Bilgilendirme veya Kampanya Mesajınız bulunmamaktadır.</center>
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        


                            <!-- 
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-envelope"></i>
                                    <h3 class="box-title">Quick Email</h3>
                                    
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="emailto" placeholder="Email to:"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject"/>
                                        </div>
                                        <div>
                                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-footer clearfix">
                                    <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                        -->
                    </section>

                    <section class="col-lg-6 connectedSortable">


                        <!-- Chat box -->
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-comments-o"></i> {{ $firma->mt->name }} ile Mesajlaşma </h3>
                            </div>
                            <div class="box-footer">
                                <form id="chat" method="post" action="/chat/mesajgonder">
                                    {{ csrf_field() }}
                                    <div class="input-group">
                                        <input type="hidden" name="mt_id" id="mt_id" value="{{ $firma->mt->id }}">
                                        <input type="hidden" name="from_id" id="from_id" value="{{ Auth::user()->id }}">
                                        <input class="form-control" name="mesaj" id="mesaj" required placeholder="Mesajınız..."/>
                                        <div class="input-group-btn">
                                            <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                                            <!--<a class="btn btn-success" onclick="gonder();"><i class="fa fa-plus"></i></a>-->
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="box-body chat" id="chat-box">
                                <div id="yazdirxx">
                                    @foreach($mesajlar as $ms)
                                    <div class="item">
                                        @if($ms->from_id==Auth::user()->id)
                                        <img src="img/avatar2.png" alt="user image" class="online"/>
                                        <p class="message">
                                            <a href="#" class="name">
                                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $ms->created_at }}</small>
                                                {{ $ms->user->name }}
                                            </a>
                                            {{ $ms->mesaj }}
                                        </p>
                                        @else
                                        <img src="img/avatar3.png" alt="user image" class="online"/>
                                        <p class="message">           
                                            <a href="#" class="name">
                                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $ms->created_at }}</small>
                                                {{ $ms->mt->name }}
                                            </a>                                            
                                            {{ $ms->mesaj }}
                                        </p>
                                        @endif
                                        
                                    </div>
                                    @endforeach
                                    {{ $mesajlar->links() }}
                                </div>
                            </div>

                        </div><!-- /.box (chat box) -->



                    </section><!-- right col -->
                </div><!-- /.row (main row) -->

            </section><!-- /.content -->
            @endsection

