@extends('layouts.app')

@section('content')
<div class="form-box" id="login-box">
    <div class="header">Egebant B2B Giriş</div>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="body bg-gray">
            @if (session()->has('flash_notification.message'))
            <div class="alert alert-{{ session('flash_notification.level') }}" style="margin-left:-15px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! session('flash_notification.message') !!}
            </div>
            @endif
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ePosta">

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control" name="password" placeholder="Şifre">

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>          
            <div class="form-group">
                <input type="checkbox" name="remember"> Beni Hatırla
            </div>
        </div>
        <div class="footer">                                                               
            <button type="submit" class="btn bg-olive btn-block">Giriş</button>  
            
            <p><a href="{{ url('/password/reset') }}">Şifremi Unuttum</a></p>
            
            <!--<a href="register.html" class="text-center">Register a new membership</a>-->
        </div>
    </form>
</div>
@endsection