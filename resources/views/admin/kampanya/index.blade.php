@extends('layouts.home')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Kampanyalar
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Yeni Kampanya Tanımla
            </button>
            <hr>
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>İlan Tipi</th>
                                <th>Segment Adı</th>
                                <th>Başlangıç</th>
                                <th>Bitiş</th>
                                <th>Tanımlama Tarihi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($kampanya as $kam)
                            <tr>
                                <td>{{ $kam->kampanya_ad }}</td>
                                <td>{{ $kam->tip }}</td>
                                <td>{{ $kam->segment->name }}</td>
                                <td>{{ tarihoku($kam->basla) }}</td>
                                <td>{{ tarihoku($kam->bitir) }}</td>
                                <td>{{ tarihoku($kam->created_at) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section><!-- /.content -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Stok Tanımlama</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/kampanya/create') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Kampanya / Bilgilendirme Başlığı</label>
                        <div class="col-md-9">
                            <input id="name" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">İlan Tipi</label>
                        <div class="col-md-9">
                            <select class="form-control" name="tip" id="tip">
                                <option value="Bilgilendirme">Bilgilendirme</option>
                                <option value="Kampanya">Kampanya</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Başlangıç Tarihi</label>
                        <div class="col-md-9">
                            <input class="form-control pull-right" id="basla" name="basla" type="text" placeholder="gg/ay/yıl (01/01/2017) gibi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Bitiş Tarihi</label>
                        <div class="col-md-9">
                            <input class="form-control pull-right" id="bitir" name="bitir" type="text" placeholder="gg/ay/yıl (01/01/2017) gibi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Hangi Segment</label>
                        <div class="col-md-9">
                            <select class="form-control" name="segment" id="segment" required>
                                <option value="">Segment Seçiniz</option>
                                @foreach($segment as $seg)
                                <option value="{{ $seg->id }}">{{ $seg->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Kampanya Açıklaması</label>
                        <div class="col-md-9">
                            <textarea id="editor1" name="editor1" rows="10" cols="80">
                                Bu alana kampanya ile ilgili açıklama yazısı eklenmeli.  
                            </textarea> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Kaydet
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

        