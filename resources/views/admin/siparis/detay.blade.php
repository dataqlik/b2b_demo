<div class="collapse.in" id="collapseExample">
	<div class="well">
		<table class="table table-striped">
			<thead>
				<th>Stok Kodu</th>
				<th>Ürün Adı</th>
				<th>Sipariş Miktarı</th>
				<th>Sipariş Birimi</th>
				<th>Birim Fiyatı</th>
				<th>Tutarı</th>
			</thead>
			<tbody>
				<?php $toplam = 0; ?>
				@foreach($sipdetay as $row)
				<tr>
					<td>{{ $row->stokkodu }}</td>
					<td>{{ $row->urun->name }}</td>
					<td>{{ $row->adet }}</td>
					<td>{{ $row->birim }}</td>
					<td>{{ $row->fiyat }}</td>
					<td>{{ $row->fiyat*$row->adet }}</td>
				</tr>
				<?php
				$toplam = $toplam + ($row->fiyat*$row->adet);
				?>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><b>Toplam</b></td>
					<td colspan="2"><b>{{ $toplam }} TL (KDV Dahil)</b></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
