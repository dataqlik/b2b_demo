<table id="example2" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Stok Kodu</th>
			<th>Stok Adı</th>
			<th>Stok Birimi</th>
			<th>Grup Kodu</th>
		</tr>
	</thead>
	<tbody>
		@foreach($stok as $st)
		<tr>
			<td>
				<form action="/admin/stok/create" method="post">
				{{ csrf_field() }}
					<input type="hidden" name="kod" id="kod" value="{{ $st->STOK_KODU }}">
					<input type="submit" name="gonder" id="gonder" value="{{ $st->STOK_KODU }}" class="btn btn-default btn-xs">
				</form>
			</td>
			<td>{{ karakter($st->STOK_ADI) }}</td>
			<td>{{ $st->OLCU_BR1 }}</td>
			<td>{{ $st->GRUP_KODU}}</td>
		</tr>
		@endforeach
	</tbody>
</table>