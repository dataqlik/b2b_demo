@extends('layouts.home')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Stok Ekle
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ url('/admin/stok/update') }}">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Stok Adı</label>
                            <input type="text" class="form-control" name="stokad" id="stokad" value="{{ karakter($stok->name) }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Stok Netsis Kodu</label>
                            <input type="text" class="form-control" name="stokkod" id="stokkod" readonly value="{{ $stok->code }}">
                        </div>
                        <div class="form-group">
                            <label>Ürün Açıklama Yazısı</label>
                            <textarea id="editor1" name="editor1" rows="10" cols="80">
                                {{ $stok->aciklama }}  
                            </textarea>        
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <input type="hidden" name="stok_id" id="stok_id" value="{{ $stok->id }}">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="callout callout-info">
                    <h4>Önemli!</h4>
                    <p>Bu listede yer alan firmalarlar B2B sisteminden tanımlı olmayabilir. Stoğa ait ek stok kodlarının listelenmesi için yer almaktadır.</p>
                </div>
                <table class="table">
                    <thead>
                        <th>Stok Kodu</th>
                        <th>Cari Kod</th>
                        <th>Firma Adı</th>
                        <th>Özel Kod</th>
                    </thead>
                    <tbody>
                       @foreach($stokkod as $st)
                       <tr>
                        <td>{{ $st->STOK_KODU }}</td>
                        <td>{{ $st->CARI_KOD }}</td>
                        <td><?php echo karakter(tblcariad($st->CARI_KOD)); ?></td>
                        <td>{{ $st->CARISTOK_KODU }}</td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>
            <hr>
            <div class="row">
                @foreach($photo as $pt)
                    <div class="col-md-4">
                    <?php echo '<img src="'.$pt->foto.'" style="width:100%" />';  ?>
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="photo_id" id="photo_id" value="{{ $pt->id }}">
                        
                            <center>
                                <input type="submit" name="sil" id="sil" class="btn btn-xs btn-danger" value="Sil">
                            </center>
                        
                    </form>
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="photo_id" id="photo_id" value="{{ $pt->id }}">
                        
                            <center>
                                @if($stok->foto_id<>$pt->id)
                                <input type="submit" name="default" id="default" class="btn btn-xs btn-warning" value="Default">
                                @endif
                            </center>
                       
                    </form>
                    </div>
                @endforeach
            </div>
            <hr>
            <form method="post" enctype="multipart/form-data" action="/admin/stok/photo/{{ $stok->id }}">
            {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" id="exampleInputFile" name="exampleInputFile">
                        <p class="help-block">Example block-level help text here.</p>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div>
</div>
</section>
@endsection

