@extends('layouts.home')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Stok Ekle
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ url('/admin/stok/save') }}">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Stok Adı (ERP Sistem)</label>
                            <input type="text" class="form-control" name="stokad" id="stokad" readonly value="{{ karakter($stok->STOK_ADI) }}">
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Stok Netsis Kodu</label>
                                <input type="text" class="form-control" name="stokkod" id="stokkod" readonly value="{{ $stok->STOK_KODU }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Şube Kodu</label>
                                <input type="text" class="form-control" name="subekod" id="subekod" readonly value="{{ $stok->SUBE_KODU }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Üretici Kodu</label>
                                <input type="text" class="form-control" name="uretici" id="uretici" readonly value="{{ $stok->URETICI_KODU }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Grup Kodu</label>
                                <input type="text" class="form-control" name="grup" id="grup" readonly value="{{ $stok->GRUP_KODU }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Kod_1</label>
                                <input type="text" class="form-control" name="kod1" id="kod1" readonly value="{{ $stok->KOD_1 }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Ölçü Birimi</label>
                                <input type="text" class="form-control" name="olcu1" id="olcu1" readonly value="{{ $stok->OLCU_BR1 }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>KDV Oranı</label>
                                <input type="text" class="form-control" name="kdv" id="kdv" readonly value="{{ $stok->KDV_ORANI }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Şu an ki Stok Miktarı</label>
                                <input type="text" class="form-control" name="miktar" id="miktar" readonly value="{{ $top }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Asgari Miktar</label>
                                <input type="text" class="form-control" name="asgari" id="asgari" readonly value="{{ $stok->ASGARI_STOK*1 }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ürün Açıklama Yazısı</label>
                            <textarea id="editor1" name="editor1" rows="10" cols="80">
                                Bu alana ürün ile ilgili açıklama yazısı eklenmeli.  
                            </textarea>        
                        </div>
                        <input type="hidden" name="olcu2" id="olcu2" value="{{ $stok->OLCU_BR2 }}">
                        <input type="hidden" name="kod2" id="kod2" value="{{ $stok->KOD_2 }}">
                        <input type="hidden" name="kod3" id="kod3" value="{{ $stok->KOD_3 }}">
                        <input type="hidden" name="kod4" id="kod4" value="{{ $stok->KOD_4 }}">
                        <input type="hidden" name="kod5" id="kod5" value="{{ $stok->KOD_5 }}">

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="callout callout-info">
                    <h4>Önemli!</h4>
                    <p>Bu listede yer alan firmalarlar B2B sisteminden tanımlı olmayabilir. Stoğa ait ek stok kodlarının listelenmesi için yer almaktadır.</p>
                </div>
                <table class="table">
                    <thead>
                        <th>Stok Kodu</th>
                        <th>Cari Kod</th>
                        <th>Firma Adı</th>
                        <th>Özel Kod</th>
                    </thead>
                    <tbody>
                     @foreach($stokkod as $st)
                     <tr>
                        <td>{{ $st->STOK_KODU }}</td>
                        <td>{{ $st->CARI_KOD }}</td>
                        <td><?php echo karakter(tblcariad($st->CARI_KOD)); ?></td>
                        <td>{{ $st->CARISTOK_KODU }}</td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>

        </div><!-- /.box -->
    </div>
</div>
</section>
@endsection

