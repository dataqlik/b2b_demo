<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<b>İşlam Başarılı !</b> Güncellemeler yapılmıştır.
</div>