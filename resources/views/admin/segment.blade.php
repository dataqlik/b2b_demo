@extends('layouts.home')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Segment Tanımlamaları
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Yeni Segment Tanımla
            </button>
            <hr>
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Segment ID</th>
                                <th>Segment Adı</th>
                                <th>Tanım</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($segment as $sgm)
                          <tr>
                              <td>{{ $sgm->id }}</td>
                              <td>{{ $sgm->name }}</td>
                              <td>{{ $sgm->tanim }}</td>
                          </tr>
                          @endforeach
                      </tbody>

                  </table>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
  </div>

</section><!-- /.content -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Segment Tanımlama</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/segment/create') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email" class="col-md-4 control-label">Segment Adı</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-4 control-label">Segment Tanımı</label>
                        <div class="col-md-6">
                            <input id="tanim" type="text" class="form-control" name="tanim">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Kaydet
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

