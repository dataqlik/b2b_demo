@extends('layouts.home')
<script type="text/javascript">
    function listeyukle()
    {
        $('#detailss').slideDown('slow');
        $("#detailss").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/admin/stok/NetsisCariListe',
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#detailss").html(cevap);
                    
                }
            });
    }
</script>
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Firma Tanımlamaları
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            
                <form method="post" class="form-inline" action="/admin/company/create">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label class="sr-only">Eklenecek Firma Cari Kodu</label>
                        <input type="text" name="carikod" id="carikod" class="form-control" placeholder="Netsis Cari Kod">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning">Getir</button>
                        <a class="btn btn-info" data-toggle="modal" data-target="#myListe" onclick="listeyukle();">Listeden Seç</a>
                        <a class="btn btn-success" data-toggle="modal" data-target="#myCustom">Özel Tanımla</a>
                    </div>
                </form>
         <hr>
        </div>
        <div class="col-xs-12">
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Cari Kod</th>
                                <th>Cari Ad</th>
                                <th>Müşteri Temsilcisi</th>
                                <th>Segment</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($comp as $cmp)
                            <tr>
                                <td><a href="/admin/company/edit/{{ $cmp->carikod }}">{{ $cmp->carikod }}</a></td>
                                <td>{{ karakter($cmp->cariad) }}</td>
                                <td>{{ $cmp->mt->name }}</td>
                                <td>{{ $cmp->segment->name }}</td>
                            </tr>
                        @endforeach 
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section><!-- /.content -->

<div class="modal fade" id="myListe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Firma Tanımlama - Listeden Seç</h4>
                </div>
                <div class="modal-body">
                    <div id="detailss">Liste Yükleme</div>
                </div>
            </div>
        </div>
    </div>
@endsection

