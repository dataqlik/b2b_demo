@extends('layouts.home')
<script type="text/javascript">
    function listeyukle()
    {
        $('#detailss').slideDown('slow');
        $("#detailss").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/admin/stok/NetsisStokListe',
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#detailss").html(cevap);
                    
                }
            });
    }
    function fiyatguncelle()
    {
        $('#guncel').slideDown('slow');
        $("#guncel").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/admin/fiyatliste/guncelle',
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#guncel").html(cevap);
                    
                }
            });
    }

    function kurguncelle()
    {
        $('#guncel1').slideDown('slow');
        $("#guncel1").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/admin/kur/guncelle',
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#guncel1").html(cevap);
                    
                }
            });
    }
    </script>

     @section('content')
     <section class="content-header">
        <h1>
            Stok Tanımlamaları
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form method="post" class="form-inline" action="/admin/stok/create">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="sr-only">Eklenecek Stok Kodu</label>
                        <input type="text" name="kod" id="kod" class="form-control" placeholder="Netsis Stok Kodu">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning">Getir</button>
                        <a class="btn btn-info" data-toggle="modal" data-target="#myListe" onclick="listeyukle();">Listeden Seç</a>
                        <a class="btn btn-success" data-toggle="modal" data-target="#myCustom">Özel Tanımla</a>
                        <a class="btn btn-danger" data-toggle="modal" data-target="#myFiyat"> Fiyat Listelerini Güncelle</a>
                        <a class="btn btn-danger" data-toggle="modal" data-target="#myKur"> Kur Bilgilerini Güncelle</a>
                    </div>
                </form>
                <hr>
            </div>
            <div class="col-xs-12">
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Stok Kodu</th>
                                    <th>Stok Adı</th>
                                    <th>Stok Birimi</th>
                                    <th>Grup Kodu</th>
                                    <th>Miktar</th>
                                    <th>Asgari</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tblstok as $st)
                                <tr>
                                    <td><a href="/admin/stok/edit/{{ $st->id }}"> {{ $st->code }}</a></td>
                                    <td>{{ $st->name }}</td>
                                    <td>{{ $st->OLCU_BR1 }}</td>
                                    <td>{{ $st->GRUP_KODU}}</td>
                                    <td>{{ $st->miktar * 1 }}</td>
                                    <td>{{ $st->dipmiktar * 1 }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->
    <div class="modal fade" id="myListe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Stok Tanımlama - Listeden Seç</h4>
                </div>
                <div class="modal-body">
                    <div id="detailss">Liste Yükleme</div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myCustom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Stok Tanımlama - Özel</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/stok/create') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Segment Adı</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Segment Tanımı</label>
                            <div class="col-md-6">
                                <input id="tanim" type="text" class="form-control" name="tanim">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Kaydet
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myFiyat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Fiyat Listeleri Güncelleme</h4>
                </div>
                <div class="modal-body">
                    <div id="guncel">
                        <div class="alert alert-warning alert-dismissable">
                            <i class="fa fa-warning"></i>
                            
                            <b>Dikkat!</b> Bu işlem sistemdeki fiyat listelerini güncelleyecektir.
                        </div>
                        <a class="btn btn-block btn-danger" onclick="fiyatguncelle();">Güncellemeyi Başlat</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myKur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Kurları Güncelleme</h4>
                </div>
                <div class="modal-body">
                    <div id="guncel1">
                        <div class="alert alert-warning alert-dismissable">
                            <i class="fa fa-warning"></i>
                            
                            <b>Dikkat!</b> Bu işlem sistemdeki tüm kur bilgileri güncelleyecektir.
                        </div>
                        <a class="btn btn-block btn-danger" onclick="kurguncelle();">Güncellemeyi Başlat</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

