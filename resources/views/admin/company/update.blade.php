@extends('layouts.home')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Firma Düzenle
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ url('/admin/company/update') }}">
                {{ csrf_field() }}
                @foreach($company as $cp)
                    <div class="box-body">
                        <div class="form-group">
                            <label>Firma Adı</label>
                            <input type="text" class="form-control" name="cariad" id="cariad" readonly value="{{ karakter($cp->cariad) }}">
                        </div>
                        <div class="form-group">
                            <label>Cari Kodu</label>
                            <input type="text" class="form-control" name="carikod" id="carikod" readonly value="{{ $cp->carikod }}">
                        </div>
                        <div class="form-group">
                            <label>Segment</label>
                            <select id="segment" name="segment" required class="form-control">
                                <option value="">Segment Seçiniz</option>
                                @foreach($segment as $seg)
                                <option  value="{{ $seg->id }}" <? if($seg->id==$cp->segment_id) { echo 'selected="selected"'; } ?> >{{ $seg->name }} ({{ $seg->tanim }})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Müşteri Temsilcisi</label>
                            <select id="mt" name="mt" required class="form-control">
                                <option value="">Müşteri Temsilcisi Seçiniz</option>
                                @foreach($mtemsil as $mt)
                                <option value="{{ $mt->id }}"  <? if($mt->id==$cp->mt_id) { echo 'selected="selected"'; } ?>>{{ $mt->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Cari Özel Kodlu Stokla çalışıyor</label>
                            <select name="stoktip" id="stoktip" class="form-control">
                                <option value="0" <? if($cp->stoktip==0) { echo 'selected="selected"'; } ?>>Hayır</option>
                                <option value="1" <? if($cp->stoktip==1) { echo 'selected="selected"'; } ?>>Evet</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Fiyat Listesi</label>
                                <select name="fiyatliste" id="fiyatliste" class="form-control" required="">
                                    <option>Seçiniz</option>
                                    <option value="BAYI">BAYI</option>
                                    @foreach($fiyat as $fy)
                                    <option value="{{ $fy->FIYATGRUBU }}" <? if($cp->fiyatlistesi==$fy->FIYATGRUBU) { echo 'selected="selected"'; } ?>>{{ $fy->FIYATGRUBU }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Fiyat Sırası</label>
                                <input type="text" name="fiyatsira" id="fiyatsira" value="{{ $cp->fiyatalani }}" class="form-control" required>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    @endforeach
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>
@endsection

