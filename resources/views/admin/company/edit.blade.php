@extends('layouts.home')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Firma Kaydet
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ url('/admin/company/save') }}">
                {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Firma Adı</label>
                            <input type="text" class="form-control" name="cariad" id="cariad" readonly value="{{ karakter($cariad) }}">
                        </div>
                        <div class="form-group">
                            <label>Cari Kodu</label>
                            <input type="text" class="form-control" name="carikod" id="carikod" readonly value="{{ $carikod }}">
                        </div>
                        <div class="form-group">
                            <label>Segment</label>
                            <select id="segment" name="segment" required class="form-control">
                                <option value="">Segment Seçiniz</option>
                                @foreach($segment as $seg)
                                <option  value="{{ $seg->id }}">{{ $seg->name }} ({{ $seg->tanim }})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Müşteri Temsilcisi</label>
                            <select id="mt" name="mt" required class="form-control">
                                <option value="">Müşteri Temsilcisi Seçiniz</option>
                                @foreach($mtemsil as $mt)
                                <option value="{{ $mt->id }}">{{ $mt->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Cari Özel Kodlu Stokla çalışıyor</label>
                            <select name="stoktip" id="stoktip" class="form-control">
                                <option value="0">Hayır</option>
                                <option value="1">Evet</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Fiyat Listesi</label>
                                <input type="text" name="fiyatliste" id="fiyatliste" value="{{ $FIYATGRUBU }}" class="form-control" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Fiyat Sırası</label>
                                <input type="text" name="fiyatsira" id="fiyatsira" value="{{ $LISTE_FIATI }}" class="form-control" required>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>
@endsection

