@extends('layouts.home')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Sistem Kulanıcıları
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
             @if (session()->has('flash_notification.message'))
            <div class="alert alert-{{ session('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! session('flash_notification.message') !!}
            </div>
            @endif
        </div>
        <div class="col-xs-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
              Bayi & Erişim Tanımla
          </button>
          <hr>
          <div class="box">
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Kullanıcı Adı</th>
                            <th>ePosta</th>
                            <th>Cari Kodu</th>
                            <th>Cari Ad</th>
                            <th>Yetki</th>
                            <th>Durum</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $usr)
                        <tr>
                            <td><a href="/admin/users/edit/{{ $usr->id }}"> {{ $usr->name }}</a></td>
                            <td>{{ $usr->email }}</td>
                            <td>@if(isset($usr->firma)) {{ $usr->firma->carikod }} @endif</td>
                            <td>@if(isset($usr->firma)) {{ $usr->firma->cariad }} @endif</td>
                            <td><? echo yetki($usr->yetki); ?></td>
                            <td>
                                <? if($usr->deleted_at) { ?>
                                    <form action="/admin/user/aktif" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="user_id" id="user_id" value="{{ $usr->id }}">
                                        <input type="submit" value="Hesap Pasif, Aktif Yap!" class="btn btn-sm btn-danger">
                                    </form>
                                <?php } else { ?>
                                    <form action="/admin/user/pasif" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="user_id" id="user_id" value="{{ $usr->id }}">
                                        <input type="submit" value="Hesap Aktif, Pasif Yap!" class="btn btn-sm btn-success">
                                    </form>
                                <?php } ?>                                                 
                            </td> 
                        </tr>
                        @endforeach   
                    </tbody>

                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

</section><!-- /.content -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px;">
        <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Bayi & Erişim Tanımlama</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/users/create') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Yetki</label>
                            <div class="col-md-6">
                                <select name="yetki" id="yetki" class="form-control">
                                    <option value="1">Superuser</option>
                                    <option value="2">Bayi</option>
                                    <option value="3">Müşteri Temsilcisi</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Cari Adı</label>
                            <div class="col-md-6">
                                <select name="carikod" id="carikod" class="form-control">
                                    <option value="0;0">Cari Seçiniz</option>
                                    @foreach($firma as $cari)
                                    <option value="{{ $cari->carikod }};{{ $cari->id }}">{{ $cari->cariad }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Kullanıdı Adı</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Posta (Sistem Kullanıcı)</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Şifre</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Şifreyi Tekrarla</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Kaydet
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection

