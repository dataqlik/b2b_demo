@extends('layouts.home')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Sistem Kulanıcıları
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/users/update') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Yetki</label>
                            <div class="col-md-6">
                                <select name="yetki" id="yetki" class="form-control">
                                    <option value="1" <? if($user->yetki=='1') { echo 'selected="selected"'; } ?>>Superuser</option>
                                    <option value="2" <? if($user->yetki=='2') { echo 'selected="selected"'; } ?>>Bayi</option>
                                    <option value="3" <? if($user->yetki=='3') { echo 'selected="selected"'; } ?>>Müşteri Temsilcisi</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Cari Adı</label>
                            <div class="col-md-6">
                                <select name="cari" id="cari" class="form-control">
                                    <option value="0;0">Cari Seçiniz</option>
                                    @foreach($company as $cari)
                                    <option value="{{ $cari->id }}" <? if($cari->id==$user->cari_id) { echo 'selected="selected"'; } ?>>{{ $cari->cariad }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Kullanıdı Adı</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Posta (Sistem Kullanıcı)</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <input type="hidden" value="{{ $user->id }}" name="uid" id="uid">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Kaydet
                                </button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <form action="/admin/users/delete" method="post">
                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $user->id }}" name="uid" id="uid">
                                    <input type="submit" class="btn btn-block btn-danger" value="Bu Kullanıcıyı Sil">
                    </form>
        </div>
    </div>
</div>

</section><!-- /.content -->

@endsection

