@extends('layouts.home')
<script type="text/javascript">
    function listeyukle(id)
    {
        $('#stokdetay').slideDown('slow');
        $("#stokdetay").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/urunler/detay/'+id,
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#stokdetay").html(cevap);
                    
                }
            });
         }
     </script>
     @section('content')
     <!-- Content Header (Page header) -->
     <section class="content-header">
        <h1>
            Kredi Kartlı Ödeme Ekranı (SanalPos)
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @if (session()->has('flash_notification.message'))
                <div class="alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                    {!! session('flash_notification.message') !!}
                </div>
                @endif
            </div>
            <div class="col-xs-6">
            <form action="/odeme/yap" method="post">
                {{ csrf_field() }}
                <div class="form-container">
                    <div class="card-wrapper"></div>
                    <input id="column-left" type="text" name="first-name" placeholder="First Name"/>
                    <input id="column-right" type="text" name="last-name" placeholder="Surname"/>
                    <input id="input-field" type="text" name="number" placeholder="Card Number"/>
                    <input id="column-left" type="text" name="expiry" placeholder="MM / YY"/>
                    <input id="column-right" type="text" name="cvc" placeholder="CCV"/>
                    <input id="input-button" type="submit" value="Ödeme Yap"/>
            </form>
        </div>
    </div>
</section><!-- /.content -->
@endsection

