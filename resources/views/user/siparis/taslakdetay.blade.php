
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Stok Kodu</th>
            <th>Stok Adı</th>
            <th>Birimi</th>
            <th>Adet</th>
        </tr>
    </thead>
    <tbody>
        @foreach($taslakdetay as $tas)
        <tr>
            <td>{{ $tas->product->code }}</td>
            <td>{{ $tas->product->name }}</td>
            <td>{{ $tas->product->OLCU_BR1 }}</td>
            <td>{{ $tas->adet }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<form action="/taslak/siparis" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="sipid" id="sipid" value="{{ $taslak->id }}">
    <input type="submit" value="Sepete Aktar" class="btn btn-success" id="send" name="send">
</form>
