@extends('layouts.home')
<script type="text/javascript">
    function listeyukle(id)
    {
        $('#siparisdetay').slideDown('slow');
        $("#siparisdetay").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/taslak/detay/'+id,
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#siparisdetay").html(cevap);
                }
            });
    }
</script>
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Taslak Siparişler
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kayıt Tarihi</th>
                                <th>Kaydeden</th>
                                <th>Açıklama</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($taslak as $tas)
                            <tr>
                                <td><a data-toggle="modal" data-target="#myModal" onclick="listeyukle({{ $tas->id }})">{{ tarihoku($tas->taslak_tarih) }}</a></td>
                                <td>{{ $tas->user->name }}</td>
                                <td>{{ $tas->aciklama }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
      </div>
  </div>

</section><!-- /.content -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Taslak Siparis Detayı</h4>
            </div>
            <div class="modal-body">
                <div id="siparisdetay"></div>
            </div>
        </div>
    </div>
</div>
@endsection

