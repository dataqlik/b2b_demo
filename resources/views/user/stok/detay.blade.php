<?php
//dd($photo);
?>
<div class="row">
	<div class="col-md-6">
		@if($photo)
		<img src="{{ $photo->foto }}" style="width:100%" />
		@else
		<img src="{{ url('img/egebant-logo.jpg') }}" style="width:100%" />
		@endif
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Stok Adı</label>
			<input type="text" class="form-control" name="stokad" id="stokad" value="{{ karakter($stok->name) }}" readonly>
		</div>
		<div class="form-group">
			<label>Ürün Açıklaması</label>
			<?php echo $stok->aciklama; ?>     
		</div>
		<div class="form-group">
			<label>&nbsp;&nbsp;&nbsp;</label>
			<form action="/urunler/ekle" method="post">
				{{ csrf_field() }}
				<input type="hidden" name="stok_id" id="stok_id" value="{{ $stok->id }}">
				<button type="submit" class="btn btn-sm btn-danger btn-block">Siparişe Ekle</button>
			</form>	     
		</div>
	</div>
</div>