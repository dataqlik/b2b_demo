@extends('layouts.home')
<script type="text/javascript">
    function listeyukle(id)
    {
        $('#stokdetay').slideDown('slow');
        $("#stokdetay").html('<center><img src="{{ url('/img/ajax-loader1.gif') }}" /></center>');
        var post_edilecek_veriler = '';
             $.ajax({ // ajax işlemi başlar
                type:'GET', 
                url:'/urunler/detay/'+id,
                datatype: 'text',
                data:post_edilecek_veriler,
                success:function(cevap){
                    $("#stokdetay").html(cevap);
                    
                }
            });
    }
</script>
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Sipariş Edilebilir Ürünler
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @if (session()->has('flash_notification.message'))
            <div class="alert alert-{{ session('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! session('flash_notification.message') !!}
            </div>
            @endif
        </div>
        <div class="col-xs-12">
        @if(count(Cart::content())>0)
            <div style="margin-bottom:5px;">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> Mevcut Siparişiniz </a>
                <a href="/urunler/sepet/bosalt" class="btn btn-danger ">Sepeti Temizle</a>
                <a href="/urunler/sepet/siparis" class="btn btn-success">Siparişi Onayla</a>
                <a data-toggle="modal" data-target="#myTaslak" class="btn btn-info">Siparişi Taslak Olarak Kaydet</a>
                <!--
                <a href="/urunler/sepet/taslak" class="btn btn-info">Siparişi Taslak Olarak Kaydet</a>
                -->
            </div>
            <div class="collapse.in" id="collapseExample">
                <div class="well">
                    <table class="table table-striped">
                        <thead>
                            <th>Stok Kodu</th>
                            <th>Ürün Adı</th>
                            <th>Sipariş Miktarı</th>
                            <th>Sipariş Birimi</th>
                            <th>Birim Fiyatı</th>
                            <th>KDV Oranı</th>
                            <th>Tutarı</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php $toplam = 0; ?>
                            @foreach(Cart::content() as $row)
                            <tr>
                                <td>{{ $row->options->stokkodu }}</td>
                                <td>{{ $row->name }}</td>
                                <td>
                                <form action="/urunler/sepet/adetguncelle/{{ $row->rowId }}" method="post" class="form-inline">
                                    <div class="input-group input-group-sm">
                                        <input type="text" value="{{ $row->qty }}" name="adet" id="adet" class="form-control">{{ csrf_field() }}
                                        <span class="input-group-btn">
                                            <input type="submit" class="btn btn-success btn-flat" name="update" id="update" value="Ok">

                                        </span>
                                    </div>
                                </form>   
                                </td>
                                <td>{{ $row->options->birim }}</td>
                                <td>{{ $row->options->doviz }} {{ $row->price }}</td>
                                <td>% {{ $row->options->tax }}</td>
                                <td>{{ $row->options->doviz }} {{ $row->price*$row->qty+($row->price*$row->qty*$row->options->tax/100) }}</td>
                                <td><a href="/urunler/sepet/sil/{{ $row->rowId }}" class="btn btn-danger btn-xs">Sil</a></td>
                            </tr>
                            <?php
                                $toplam = $toplam + $row->price*$row->qty+($row->price*$row->qty*$row->options->tax/100);
                                $kur = $row->options->kur;
                                $doviz = $row->options->doviz;
                            ?>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td colspan="2"><b>Hesaplanan Döviz : {{ $doviz }}</b> / <b>Güncel Kur : {{ $kur }}</b></td>
                                <td colspan="2"></td>
                                <td><b>Toplam</b></td>
                                <td colspan="2"><b>{{ round($toplam*$kur,2) }} TL</b></td>
                            </tr>
                            <tr>
                                <td colspan="7"><b>Birim fiyatı ERR! -9999 yazan spiariş kalemlerinin fiyatları müşteri temsilcisi tarafından girilecektir.</b></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <hr>
        @endif
            <div class="box box-solid">
                <div class="box-body">
                    <table class="table" id="example1">
                        <thead>
                            <th>Stok Kodu</th>
                            <th>Stok Adı</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($tblstok as $st)
                            <?php 
                            if($stoktip==1) { 
                                $durum   = 0;
                                $kontrol = explode(';',$st->custom);
                                $say     = count($kontrol);
                                for($i=0;$i<$say;$i++)
                                {
                                    $dd = explode('|',$kontrol[$i]);
                                    //dd($dd);
                                    if($dd[0]==$firmakod)
                                    {
                                        $durum=1;
                                    }
                                }
                                if($durum>0) { 
                            ?>
                            <tr>
                                <td>{{ $st->code }}</td>
                                <td><a data-toggle="modal" data-target="#myModal" onclick="listeyukle({{ $st->id }})">{{ $st->name }}</a></td>
                                <td>{{ $st->KOD_1 }}</td>
                                <td>{{ $st->KOD_2  }}</td>
                                <td>{{ $st->KOD_3  }}</td>
                                <td>{{ $st->KOD_4  }}</td>
                                <td>{{ $st->KOD_5  }}</td>
                                <td> 
                                <?php if($st->miktar>$st->dipmiktar) { ?>
                                    <form action="/urunler/ekle" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="stok_id" id="stok_id" value="{{ $st->id }}">
                                        <button type="submit" class="btn btn-sm btn-success btn-block">Siparişe Ekle</button>
                                    </form>
                                <?php } else { echo '<a class="btn btn-danger btn-sm btn-block">Yetersiz Stok</a>';  } ?> 
                                </td>
                            </tr>
                            <?php } } else { ?>
                                <tr>
                                <td>{{ $st->code }}</td>
                                <td><a data-toggle="modal" data-target="#myModal" onclick="listeyukle({{ $st->id }})">{{ $st->name }}</a></td>
                                <td>{{ $st->KOD_1 }}</td>
                                <td>{{ $st->KOD_2  }}</td>
                                <td>{{ $st->KOD_3  }}</td>
                                <td>{{ $st->KOD_4  }}</td>
                                <td>{{ $st->KOD_5  }}</td>
                                <td> 
                                     <?php if($st->miktar>$st->dipmiktar) { ?>
                                    <form action="/urunler/ekle" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="stok_id" id="stok_id" value="{{ $st->id }}">
                                        <button type="submit" class="btn btn-sm btn-success btn-block">Siparişe Ekle</button>
                                    </form>
                                <?php } else { echo '<a class="btn btn-danger btn-sm btn-block">Yetersiz Stok</a>';  } ?> 
                                </td>
                            </tr>
                            <?php }?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Stok Detayı</h4>
            </div>
            <div class="modal-body">
                <div id="stokdetay"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myTaslak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Taslak Sipariş Onayı</h4>
            </div>
            <div class="modal-body">
                <form action="/urunler/sepet/taslak" method="post">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label>Taslak Açıklaması</label>
                        <textarea class="form-control" name="aciklama" id="aciklama"></textarea>
                    </div>
                    <input type="submit" class="btn btn-success" id="send" name="send" value="Kaydet">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

