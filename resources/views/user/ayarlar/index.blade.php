@extends('layouts.home')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Profil Ayarları
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @if (session()->has('flash_notification.message'))
            <div class="alert alert-{{ session('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {!! session('flash_notification.message') !!}
            </div>
            @endif
        </div>

        <div class="col-xs-6">
            <div class="box box-solid box-info">
                    <div class="box-header">
                        <h5 class="box-title" style="text-transform: capitalize">Profil Bilgileri</h5>
                    </div><!-- /.box-header -->
                    <form action="/ayarlar/kaydet" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Adınız</label>
                                <input type="text" value="{{ $user->name }}" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>E-Posta</label>
                                <input type="text" value="{{ $user->email }}" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Yeni Şifre</label>
                                <input type="password" class="form-control" name="pass" id="pass" value="">
                            </div>
                            <div class="form-group">
                                <label>Yeni Şifre Tekrarı</label>
                                <input type="password" class="form-control"  name="pass2" id="pass2" value="">
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                        {{ csrf_field() }}
                            <input type="hidden" id="id" name="id" value="{{ $user->id }}">
                            <input type="submit" class="btn btn-info" value="Kaydet" name="kaydet">
                        </div>
                    </form>
                </div>
        </div>
    </div>
</section><!-- /.content -->
@endsection

