<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taslak extends Model
{
    protected $table = 'taslaksiparis';

    public function detay()
    {
    	return $this->hasMany('App\TaslakDetay', 'tsip_id', 'id');
    }

    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
