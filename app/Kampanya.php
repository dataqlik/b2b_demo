<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Kampanya extends Model
{
    protected $table = 'kampanya';

    public function segment()
    {
        return $this->hasOne('App\Segment', 'id', 'segment_id');
    }
}
