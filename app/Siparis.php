<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Siparis extends Model
{
    protected $table = 'siparis';

    public function firma()
    {
        return $this->hasOne('App\Company', 'id', 'cari_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function mt()
    {
        return $this->hasOne('App\User', 'id', 'mt_id');
    }

    public function drm()
    {
    	return $this->hasOne('App\Durum','tip','durum');
    }

    public function detay()
    {
        return $this->hasOne('App\SiparisDetay','siparis_id','id');
    }
    
}
