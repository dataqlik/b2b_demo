<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiyatListe extends Model
{
    protected $table = 'fiyatliste';

    public function doviz()
    {
        return $this->hasOne('App\KurTipleri', 'SIRA', 'FIYATDOVIZTIPI');
    }

    public function kur()
    {
        return $this->hasOne('App\Kur', 'SIRA', 'FIYATDOVIZTIPI');
    }
}
