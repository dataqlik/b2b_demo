<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


    public function foto()
    {
        return $this->hasOne('App\Photo', 'stok_id', 'id');
    }

    public function defaultfoto()
    {
        return $this->hasOne('App\Photo', 'id', 'foto_id');
    }
}
