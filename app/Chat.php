<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
	protected $table = 'chatlog';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function mt()
    {
        return $this->hasOne('App\User', 'id', 'mt_id');
    }

    public function from()
    {
        return $this->hasOne('App\User', 'id', 'from_id');
    }
}
