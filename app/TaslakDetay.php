<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaslakDetay extends Model
{
    protected $table = 'taslaksiparisdetay';

    public function product()
    {
    	return $this->hasOne('App\Stok','code','stokkodu');
    }
}
