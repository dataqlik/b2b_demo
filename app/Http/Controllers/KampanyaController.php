<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Kampanya;
use App\User;
use App\Segment;
use App\Stok;

class KampanyaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kampanya = Kampanya::with('segment')->get();
        $segment  = Segment::get();
        $stok     = Stok::where('durum',0)->get();
        return view('admin.kampanya.index',compact('kampanya','segment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        DB::table('kampanya')->insert([
            'kampanya_ad' => $_POST['name'], 
            'segment_id' => $_POST['segment'],
            'aciklama' => $_POST['editor1'],
            'basla' => tarihyaz($_POST['basla']),
            'bitir' => tarihyaz($_POST['bitir']),
            'tip' => $_POST['tip'],
            'created_at' => date('Y-m-d H:i:s')
            ]); 
        return redirect('/admin/kampanya');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
