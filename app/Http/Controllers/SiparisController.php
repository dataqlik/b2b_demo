<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Cart;
use DB;
use App\User;
use App\Kur;
use App\KurTipleri;
use App\Stok;
use App\Siparis;
use App\SiparisDetay;
use App\Durum;
use App\FiyatListe;
use App\Company;
use App\Taslak;
use App\TaslakDetay;
use Mail;


class SiparisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('firma')->find(Auth::user()->id);
        $siparis = Siparis::with('firma','user','drm')->where('cari_id',$user->firma->id)->orderby('id','desc')->get();
        return view('user.siparis.index',compact('siparis'));
    }

    public function mtindex()
    {
        $mt = User::find(Auth::user()->id);
        $siparis = Siparis::with('firma','user','drm')->where('durum','0')->orwhere('durum','5')->where('mt_id',Auth::user()->id)->get();
        return view('mt.siparis.index',compact('siparis'));
    }

    public function adminindex()
    {
        $siparis = Siparis::with('firma','user','drm','mt')->where('durum','0')->orwhere('durum','5')->get();
        return view('admin.siparis.index',compact('siparis'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kurtarih = date('Y-m-d');
        $user = User::with('firma')->find(Auth::user()->id);
        $company = Company::with('mt')->where('id',$user->cari_id)->first();
        $cart = Cart::content();
        $total = Cart::total();

        $sipkod = Siparis::orderby('id','desc')->first();
        if(count($sipkod)==0)
        {
            $erpkodu = 'B2B'.date('Y').'00000001';
        }
        else
        {
            $sira       = substr($sipkod->erpkodu,8,8) * 1;
            $sirason    = $sira + 1;

            switch(count($sirason))
            {
                case '1' : $sirayaz = '0000000'.$sirason; break;
                case '2' : $sirayaz = '00000'.$sirason; break;
                case '3' : $sirayaz = '0000'.$sirason; break;
                case '4' : $sirayaz = '000'.$sirason; break;
                case '5' : $sirayaz = '00'.$sirason; break;
                case '6' : $sirayaz = '0'.$sirason; break;
                case '7' : $sirayaz = $sirason; break;
            }

            $erpkodu = 'B2B'.date('Y').$sirayaz;
        }

        DB::table('siparis')->insert([
            'cari_id' => $user->firma->id, 
            'carikod' => $user->firma->carikod,
            'user_id' => $user->id,
            'mt_id'   => $user->firma->mt_id, 
            'tutar' => paracevir($total),
            'erpkodu' => $erpkodu,
            'fiyat_tarihi' => date('Y-m-d H:i:s'),
            'durum' => 9, 
            'created_at' => date('Y-m-d H:i:s')
            ]);
        $siparis = Siparis::where('cari_id',$user->firma->id)->where('carikod',$user->firma->carikod)->where('user_id',$user->id)->where('durum',9)->first();
        $tutar = 0;
        foreach($cart as $sip)
        {
            DB::table('siparisdetay')->insert([
                'siparis_id'=> $siparis->id,
                'stokkodu'  => $sip->options->stokkodu,
                'adet'      => $sip->qty,
                'fiyat'     => $sip->price,
                'tutar'     => $sip->qty * $sip->price,
                'indirimoran' => '0',
                'indirimtutar'=> '0',
                'doviz'     =>  $sip->options->doviz,
                'kur'       =>  $sip->options->kur,
                'kdv'       =>  $sip->options->tax,
                'birim'=> $sip->options->birim,
                'created_at' => date('Y-m-d H:i:s')
                ]);
            $tutar = $tutar +  ( ( $sip->qty * $sip->price ) * ( 1 + ( $sip->options->tax / 100 ) ) * $sip->options->kur );
        }
        Cart::destroy();
        DB::table('siparis')->where('id', $siparis->id)->update([
            'durum' => '0', 
            'tutar' => $tutar,
            'updated_at' => date('Y-m-d H:i:s')
            ]);

        $data = ['ad' => $company->mt->name, 'username' => $company->mt->email ];
        Mail::send('emails.siparisgeldi', $data, function($message) use($data)
        {
            $message->to($data['username'], $data['ad'])
            ->subject('Egebant B2B - Yeni Sipariş Var.')
            ->replyTo('egebant@egebant.com.tr', 'Egebant');
        });


        return redirect('/siparis');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function taslak(Request $request)
    {
        $user = User::with('firma')->find(Auth::user()->id);
        $company = Company::with('mt')->where('carikod',$user->carikod);
        $cart = Cart::content();
        DB::table('taslaksiparis')->insert([
            'cari_id' => $user->firma->id, 
            'carikod' => $user->firma->carikod,
            'user_id' => $user->id, 
            'aciklama' => $_POST['aciklama'],
            'taslak_tarih' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s')
            ]);
        $taslaksiparis = Taslak::where('cari_id',$user->firma->id)->where('carikod',$user->firma->carikod)->where('user_id',$user->id)->orderby('id','desc')->first();
        foreach($cart as $sip)
        {
            DB::table('taslaksiparisdetay')->insert([
                'tsip_id'=> $taslaksiparis->id,
                'stokkodu'  => $sip->options->stokkodu,
                'adet'      => $sip->qty,
                'birim'=> $sip->options->birim,
                'created_at' => date('Y-m-d H:i:s')
                ]);
        }
        $taslak = Taslak::with('detay','user')->get();
        Cart::destroy();
        flash('Sepetteki ürünler taslak sipariş olarak sisteme kaydedilmiştir.');
        return redirect('/urunler');
    }

    public function taslakindex()
    {
        $user = User::with('firma')->find(Auth::user()->id);
        $company = Company::with('mt')->where('carikod',$user->carikod);
        $taslak = Taslak::with('user')->where('cari_id',$user->firma->id)->where('carikod',$user->firma->carikod)->orderby('id','desc')->get();
        return view('user.siparis.taslak',compact('taslak'));
    }

    public function taslakdetay($id)
    {
        $user = User::with('firma')->find(Auth::user()->id);
        $company = Company::with('mt')->where('carikod',$user->carikod);
        $taslak = Taslak::where('cari_id',$user->firma->id)->where('carikod',$user->firma->carikod)->where('id',$id)->first();
        $taslakdetay = TaslakDetay::where('tsip_id',$taslak->id)->with('product')->get();
        return view('user.siparis.taslakdetay',compact('taslak','taslakdetay'));
    }

    public function taslaktosiparis(Request $request)
    {
        $user = User::with('firma')->find(Auth::user()->id);
        $company = Company::with('mt')->where('carikod',$user->carikod)->first();
        $taslak = Taslak::where('cari_id',$user->firma->id)->where('carikod',$user->firma->carikod)->where('id',$_POST['sipid'])->first();
        $taslakdetay = TaslakDetay::where('tsip_id',$taslak->id)->with('product')->get();

        foreach ($taslakdetay as $tas) 
        {
            $stok       = Stok::find($tas->product->id);
            $stfiyat    = FiyatListe::where('FIYATGRUBU',$company->fiyatlistesi)->where('STOKKODU',$stok->code)->with('doviz')->first();
            if($stfiyat)
            {
                switch ($company->fiyatalani) 
                {
                    case '1': $fiyat=$stfiyat->FIYAT1; break;
                    case '2': $fiyat=$stfiyat->FIYAT2; break;
                    case '3': $fiyat=$stfiyat->FIYAT3; break;
                    case '4': $fiyat=$stfiyat->FIYAT4; break;
                    default : $fiyat=$stfiyat->FIYAT1;     
                }
                $doviz = $stfiyat->doviz->ISIM;
                $kur = Kur::where('SIRA',$stfiyat->FIYATDOVIZTIPI)->where('TARIH',date('Y-m-d'))->first();
                $guncel = $kur->EFF_SATIS;
            } 
            else
            {
                $stfiyat1    = FiyatListe::where('FIYATGRUBU','GNL')->where('STOKKODU',$stok->code)->with('doviz')->first();
                if($stfiyat1)
                {
                    switch ($company->fiyatalani) 
                    {
                        case '1': $fiyat=$stfiyat1->FIYAT1; break;
                        case '2': $fiyat=$stfiyat1->FIYAT2; break;
                        case '3': $fiyat=$stfiyat1->FIYAT3; break;
                        case '4': $fiyat=$stfiyat1->FIYAT4; break;
                        default : $fiyat=$stfiyat1->FIYAT1;
                    }
                    $doviz = $stfiyat1->doviz->ISIM;
                    $kur = Kur::where('SIRA',$stfiyat1->FIYATDOVIZTIPI)->where('TARIH',date('Y-m-d'))->first();
                    $guncel = $kur->EFF_SATIS;
                }
                else
                {
                    $fiyat = -9999;
                    $doviz = 'ERR!';
                    $guncel = 0;
                }
            }
            Cart::add($stok->id, $stok->name, 1, $fiyat, ['doviz'=> $doviz,'kur'=>$guncel,'birim' => $stok->OLCU_BR1,'stokkodu' => $stok->code ,'tax'=>$stok->kdv]);
        }
        flash('Taslak Siparişiniz Sepete eklendi.');
        return redirect('/urunler');
    }

    public function sepeteekle(Request $request)
    {
        $user       = User::find(Auth::user()->id);
        $company    = Company::find($user->cari_id);
        $stok       = Stok::find($_POST['stok_id']);
        //$stfiyat    = FiyatListe::where('FIYATGRUBU',$company->fiyatlistesi)->where('STOKKODU',$stok->code)->with('doviz')->first();
        $stfiyat    = FiyatListe::where('FIYATGRUBU',$company->fiyatlistesi)->where('STOKKODU',$stok->code)->with('doviz')->first();
        if(count($stfiyat)>0)
        {
            switch ($company->fiyatalani) 
            {
                case '1': $fiyat=$stfiyat->FIYAT1; break;
                case '2': $fiyat=$stfiyat->FIYAT2; break;
                case '3': $fiyat=$stfiyat->FIYAT3; break;
                case '4': $fiyat=$stfiyat->FIYAT4; break;
                default : $fiyat=$stfiyat->FIYAT1;     
            }
            $doviz = $stfiyat->doviz->ISIM;
            $kur = Kur::where('SIRA',$stfiyat->FIYATDOVIZTIPI)->where('TARIH',date('Y-m-d'))->first();
            if($kur)
                {
                    $guncel = $kur->EFF_SATIS;
                }
                else
                {
                    $kurson = Kur::where('SIRA',$stfiyat->FIYATDOVIZTIPI)->orderby('TARIH','desc')->first();
                    $guncel = $kurson->EFF_SATIS;
                }
        } 
        else
        {
            $stfiyat1 = FiyatListe::where('FIYATGRUBU','BAYI')->where('STOKKODU',$stok->code)->with('doviz')->first();
            if($stfiyat1)
            {
                switch ($company->fiyatalani) 
                {
                    case '1': $fiyat=$stfiyat1->FIYAT1; break;
                    case '2': $fiyat=$stfiyat1->FIYAT2; break;
                    case '3': $fiyat=$stfiyat1->FIYAT3; break;
                    case '4': $fiyat=$stfiyat1->FIYAT4; break;
                    default : $fiyat=$stfiyat1->FIYAT1;
                }
                $doviz = $stfiyat1->doviz->ISIM;
                $kur = Kur::where('SIRA',$stfiyat1->FIYATDOVIZTIPI)->where('TARIH',date('Y-m-d'))->first();
                if($kur)
                {
                    $guncel = $kur->EFF_SATIS;
                }
                else
                {
                    $kurson = Kur::where('SIRA',$stfiyat1->FIYATDOVIZTIPI)->orderby('TARIH','desc')->first();
                    $guncel = $kurson->EFF_SATIS;
                }
            }
            else
            {
                $fiyat = -9999;
                $doviz = 'ERR!';
                $guncel = 0;
            }
        }
        Cart::add($stok->id, $stok->name, 1, $fiyat, ['doviz'=> $doviz,'kur'=>$guncel,'birim' => $stok->OLCU_BR1,'stokkodu' => $stok->code ,'tax'=>$stok->kdv]);
        return redirect('/urunler');
    } 

    public function adetguncelle(Request $request,$id)
    {
        Cart::update($id, $_POST['adet']);
        return redirect('/urunler');
    }   


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function Listindex($id)
    {
        $siparis    = Siparis::with('detay')->find($id);
        $sipdetay   = SiparisDetay::with('urun')->where('siparis_id',$id)->get();
        return view('user.siparis.detay',compact('siparis','sipdetay'));
    }

    public function AdminListindex($id)
    {
        $siparis    = Siparis::with('detay')->find($id);
        $firma      = Company::find($siparis->cari_id);
        $sipdetay   = SiparisDetay::with('urun')->where('siparis_id',$id)->get();
        return view('user.siparis.detay',compact('siparis','sipdetay','firma'));
    }

    public function MtListindex($id)
    {
        $siparis    = Siparis::with('detay')->find($id);
        $firma      = Company::find($siparis->cari_id);
        $sipdetay   = SiparisDetay::with('urun')->where('siparis_id',$id)->get();
        return view('mt.siparis.detay',compact('siparis','sipdetay','firma'));
    }

    public function Mtsiparis($id)
    {
        $siparis    = Siparis::with('detay','user')->find($id);
        $firma      = Company::with('mt')->find($siparis->cari_id);
        $sipdetay   = SiparisDetay::with('urun','sipdetay')->where('siparis_id',$id)->get();
        $fdetay = DB::connection('LS')->select('select top 1 CM_BORCT,CM_ALACT,RISK_SINIRI from TBLCASABIT where CARI_KOD=?',array($firma->carikod));
        return view('mt.siparis.incele',compact('siparis','sipdetay','firma','fdetay'));
    }
    public function MtsiparisBasla($id)
    {
        DB::table('siparis')->where('id', $id)->update([
            'durum' => '5', 
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/mt/siparis/islem/'.$id);
    }
    public function MtSiparisKalemIptal($id,$urun)
    {
        DB::table('siparisdetay')->where('siparis_id',$id)->where('id',$urun)->update([
            'durum' => '2',
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/mt/siparis/islem/'.$id);
    }
    public function MtSiparisKalemOnay($id,$urun)
    {
        DB::table('siparisdetay')->where('siparis_id',$id)->where('id',$urun)->update([
            'durum' => '1',
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/mt/siparis/islem/'.$id);
    }
    public function MtSiparisKalemTemin($id,$urun)
    {
        DB::table('siparisdetay')->where('siparis_id',$id)->where('id',$urun)->update([
            'durum' => '3',
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/mt/siparis/islem/'.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Cart::remove($id);
        return redirect('/urunler');
    }

    public function sepetdestroy()
    {
        Cart::destroy();
        return redirect('/urunler');
    }


    public function erpaktar(Request $request)
    {
        $siparis = Siparis::with('detay','firma','user','mt')->where('erpkodu',$_POST['siparis'])->first();
        $company = Company::find($siparis->firma->id);
        /* Sipariş Üst Bilgisi İşleniyor (TBLSIPAMAS) */
        
        $sipust = DB::connection('LS')->insert('INSERT INTO TBLSIPAMAS (SUBE_KODU, FTIRSIP, FATIRS_NO, CARI_KODU, TARIH, TIPI, BRUTTUTAR, KDV, ACIKLAMA, KDV_DAHILMI, FATKALEM_ADEDI, TOPDEPO, GENELTOPLAM, DOVIZTIP, DOVIZTUT, KAYITYAPANKUL, KAYITTARIHI, ISLETME_KODU, KOD1, KOD2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',array(
            '0',
            '6',
            $siparis->erpkodu,
            $siparis->carikod,
            date('Y-m-d H:i:s'),
            '2',
            $siparis->tutar - ($siparis->tutar / 1.18),
            ($siparis->tutar / 1.18),
            $siparis->carikod,
            'H',
            count($siparis->detay),
            '51',
            $siparis->tutar,
            '0',
            '0',
            'B2B',
            $siparis->created_at,
            '1',
            $_POST['kod1'],
            $_POST['kod2']
            ));
        
        /* Sipariş Kalemleri işleniyor (TBLSIPATRA) */

        $siparisdetay = SiparisDetay::with('urun','sipdetay')->where('siparis_id',$siparis->id)->get();
        $sirano = 1;
        foreach($siparisdetay as $sp)
        {
            if($sp->sipdetay->durum_id==1)
            {
                $kurbilgi = KurTipleri::where('ISIM',$sp->doviz)->first();
                $sipkalem = DB::connection('LS')->insert('INSERT INTO TBLSIPATRA (STOK_KODU, FISNO, STHAR_GCMIK, STHAR_GCKOD, STHAR_TARIH, STHAR_NF, STHAR_BF, STHAR_KDV, DEPO_KODU, STHAR_ACIKLAMA, STHAR_FTIRSIP, LISTE_FIAT, STHAR_HTUR, STHAR_KOD1, STHAR_KOD2, STHAR_CARIKOD, STHAR_SIP_TURU, SIRA, STRA_SIPKONT, BAGLANTI_NO, SUBE_KODU, FIYATTARIHI, STHAR_BGTIP, STHAR_DOVTIP, STHAR_DOVFIAT ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', array(
                    $sp->stokkodu,
                    $siparis->erpkodu,
                    $sp->adet,
                    'C',
                    date('Y-m-d H:i:s'),
                    $sp->tutar * $sp->kur,
                    $sp->tutar * $sp->kur,
                    $sp->kdv,
                    '51',
                    $siparis->carikod,
                    '6',
                    $company->fiyatalani,
                    'H',
                    'I',
                    '4',
                    $siparis->carikod,
                    'B',
                    $sirano,
                    $sirano,
                    '0',
                    '0',
                    date('Y-m-d H:i:s'),
                    'I',
                    $kurbilgi->SIRA,
                    $sp->kur
                    ));
                $sirano++;
            }
        }
        DB::table('siparis')->where('erpkodu', $siparis->erpkodu)->update([
            'durum' => '4', 
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/mt/siparisler');
    }
}
