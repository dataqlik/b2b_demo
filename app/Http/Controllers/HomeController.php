<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Chat;
use App\Stok;
use App\Siparis;
use App\Kampanya;
use App\Kur;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return DB::connection('LS')->select('SELECT TOP 1 *  FROM [AKADEMI05].[dbo].[TBLCASABIT]');
        $user = User::with('firma')->find(Auth::user()->id);
        if($user->deleted_at)
        {
            auth()->logout();
            flash()->error('Hesabınız Pasif durumda lütfen Müşteri Temsilciniz ile görüşünüz.');
            return back();
        }
        else
        {
            if($user->yetki==1)
            {
                return view('admin',compact('user'));
            }
            elseif($user->yetki==3)
            {
                //$chatlog    = Chat::with('user','mt')->where('mt_id',Auth::user()->id)->where('okundu',0)->orderby('id','desc')->groupBy('user_id')->get();
                //$comp       = Company::with('segment')->where('mt_id',Auth::user()->id)->get();
                //$siparis    = Siparis::with('firma','user','drm')->where('durum','0')->orwhere('durum','5')->where('mt_id',Auth::user()->id)->get();
                
    
                //$mesajsay   = count($chatlog);
                //$siparissay = count($siparis);
                //$firmasay   = count($comp);
              
                return view('mt',compact('user'));
            }
            else
            {
                $mesajlar = Chat::with('user','mt')->where('user_id',Auth::user()->id)->orderby('id','desc')->simplePaginate(5);
                $firma = Company::with('mt')->where('id',$user->cari_id)->first();
                //$fdetay = DB::connection('LS')->select('select top 1 * from TBLCASABIT where CARI_KOD=?',array($firma->carikod));
                $kampanya = Kampanya::where('basla','<',date('Y-m-d'))->where('bitir','>',date('Y-m-d'))->where('segment_id','8')->orwhere('segment_id',$firma->segment_id)->orderby('id','desc')->get();
                
                $siparis    = Siparis::with('drm')->where('user_id',Auth::user()->id)->get();
                $usdkur     = Kur::where('SIRA','1')->orderby('TARIH','desc')->first();
                $eurokur    = Kur::where('SIRA','7')->orderby('TARIH','desc')->first();
                $gbpkur     = Kur::where('SIRA','5')->orderby('TARIH','desc')->first();
                $siparissay = count($siparis);
                $usd        = 'USD  : '.tarihoku($usdkur->TARIH).' -- '.round($usdkur->EFF_SATIS,4);
                $euro       = 'EURO : '.tarihoku($eurokur->TARIH).' -- '.round($eurokur->EFF_SATIS,4);
                $gbp        = 'GBP  : '.tarihoku($gbpkur->TARIH).' -- '.round($gbpkur->EFF_SATIS,4);
                return view('home',compact('user','firma','mesajlar','kampanya','usd','euro','gbp','siparis'));
            }
        }
    }

    
}
