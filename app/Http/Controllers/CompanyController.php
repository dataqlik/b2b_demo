<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Segment;
use App\Company;
use App\User;
use Auth;
use App\FiyatListe;
use DB;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comp = Company::with('segment','mt')->get();
        return view('admin.company',compact('comp'));
    }

    public function mtindex()
    {
        $comp = Company::with('segment')->where('mt_id',Auth::user()->id)->get();
        return view('mt.company.index',compact('comp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $segment = Segment::get();
        $mtemsil = User::where('yetki',3)->get();
        $fiyat   = FiyatListe::select('FIYATGRUBU')->distinct()->get();
        return $company = DB::connection('LS')->select('select CARI_KOD,CARI_ISIM,M_KOD,PLASIYER_KODU,FIYATGRUBU,LISTE_FIATI from TBLCASABIT where CARI_KOD=?',array($_POST['carikod']));
        foreach ($company  as $cp) {
            $cariad = $cp->CARI_ISIM;
            $carikod= $cp->CARI_KOD;
            $FIYATGRUBU= $cp->FIYATGRUBU;
            $LISTE_FIATI= $cp->LISTE_FIATI;
        }
        return view('admin.company.edit',compact('cariad','carikod','segment','mtemsil','FIYATGRUBU','LISTE_FIATI','fiyat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cdetails = Company::where('carikod',$_POST['carikod'])->get();// DB::select('select * from companies where carikod=?',array($_POST['carikod']));
        if(count($cdetails)>0)
        {
            DB::table('companies')->where('carikod', $_POST['carikod'])->update([
            'mt_id' => $_POST['mt'], 
            'segment_id' => $_POST['segment'],
            'updated_at' => date('Y-m-d H:i:s')
            ]);
            return redirect('/admin/company');
        }
        else
        {
            DB::table('companies')->insert([
            'carikod' => $_POST['carikod'],
            'cariad' => $_POST['cariad'], 
            'mt_id' => $_POST['mt'], 
            'segment_id' => $_POST['segment'],
            'stoktip' => $_POST['stoktip'],
            'fiyatlistesi' => $_POST['fiyatliste'],
            'fiyatalani' => $_POST['fiyatsira'],
            'created_at' => date('Y-m-d H:i:s')
            ]); 
            return redirect('/admin/company');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::where('carikod',$id)->get();
        $mtemsil = User::where('yetki','3')->get();
        $segment = Segment::get();
        $fiyat   = FiyatListe::select('FIYATGRUBU')->distinct()->get();
        return view('admin.company.update',compact('company','mtemsil','segment','fiyat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         DB::table('companies')->where('carikod', $_POST['carikod'])->update([
            'mt_id'         => $_POST['mt'], 
            'segment_id'    => $_POST['segment'],
            'stoktip'       => $_POST['stoktip'],
            'fiyatlistesi'  => $_POST['fiyatliste'],
            'fiyatalani'    => $_POST['fiyatsira'],
            'updated_at'    => date('Y-m-d H:i:s')
            ]);
            return redirect('/admin/company');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
