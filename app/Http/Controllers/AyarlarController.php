<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\User;
use Auth;

class AyarlarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('firma')->find(Auth::user()->id);

        return view('user.ayarlar.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        //$user = User::with('firma')->find(Auth::user()->id);
        if($_POST['pass']<>'')
        {
            if($_POST['pass']<>$_POST['pass2'])
            {
                DB::table('users')->where('id', $_POST['id'])->update([
                    'name' => $_POST['name'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                flash('Şifreler aynı değil!');
            }
            else
            {
                DB::table('users')->where('id', $_POST['id'])->update([
                    'name' => $_POST['name'],
                    'password' => bcrypt($_POST['pass']),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                flash('Şifreniz ve Profiliniz Güncelledi!');
            }
        }
        else
        {
            DB::table('users')->where('id', $_POST['id'])->update([
                    'name' => $_POST['name'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                flash('Profiliniz Güncelledi!');
        }
        return redirect('/ayarlar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
