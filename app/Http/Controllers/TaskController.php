<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use DB;
use App\Chat;
use App\User;
use App\Company;
use App\Stok;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mailindex()
    {
        return $mailler = Chat::where('mt_id',Auth::user()->id)->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function NetsisStokMiktar()
    {
        return $stok = Stok::get();
        foreach ($stok as $st) 
        {
            $stmik = DB::connection('LS')->select('select SUBE_KODU,ISLETME_KODU,STOK_KODU,URETICI_KODU,STOK_ADI,GRUP_KODU,KOD_1,KOD_2,KOD_3,KOD_4,KOD_5,OLCU_BR1,OLCU_BR2,KDV_ORANI,ASGARI_STOK from TBLSTSABIT where STOK_KODU=?',array($st->code));
            foreach($stmik as $stm)
            {
                $okundu = DB::table('stoks')
                ->where('code',$stm->STOK_KODU)
                ->update(['miktar' => 1]);
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
