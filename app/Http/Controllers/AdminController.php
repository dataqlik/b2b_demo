<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Redirect;
use App\User;
use App\Segment;
use App\Company;
use App\FiyatListe;
use App\Doviz;
use App\KurTipleri;
use DB;
use Auth;
use Mail;

class AdminController extends Controller
{
    public function users()
    {
      $users = User::with('firma')->get();
      $firma = Company::get();  
      return view('admin.users',compact('users','firma'));
    }

    public function editUser($id)
    {
        $user     = User::with('firma')->find($id);
        $company   = Company::get(); 
        return view('admin.user.edit',compact('user','company'));
    }

    public function deleteUser(Request $request)
    {
        DB::table('users')->where('id', $_POST['uid'])->delete();
        $users = User::with('firma')->get();
        $firma = Company::get();  
        return view('admin.users',compact('users','firma'));
    }

    public function updateUser(Request $request)
    {
        DB::table('users')->where('id', $_POST['uid'])->update([
            'name' => $_POST['name'], 
            'email' => $_POST['email'],
            'cari_id' => $_POST['cari'],
            'yetki' => $_POST['yetki'],
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        $users = User::with('firma')->get();
        $firma = Company::get();  
        return view('admin.users',compact('users','firma'));
    }

    public function Netsisfiyatguncelle()
    {
        set_time_limit(0);
        
            FiyatListe::truncate();
            $fiyatlar = DB::connection('LS')->select('SELECT INCKEYNO,STOKKODU,A_S,FIYAT1,FIYAT2,FIYAT3,FIYAT4,FIYATDOVIZTIPI,BASTAR,BITTAR,FIYATGRUBU FROM TBLSTOKFIAT');
            
            foreach($fiyatlar as $fy)
            {
                DB::table('fiyatliste')->insert([
                    'INCKEYNO'  => $fy->INCKEYNO,
                    'STOKKODU'  => $fy->STOKKODU,
                    'A_S'       => $fy->A_S,
                    'FIYAT1'    => round($fy->FIYAT1,3),
                    'FIYAT2'    => round($fy->FIYAT2,3),
                    'FIYAT3'    => round($fy->FIYAT3,3),
                    'FIYAT4'    => round($fy->FIYAT4,3),
                    'FIYATDOVIZTIPI'  => $fy->FIYATDOVIZTIPI,
                    'BASTAR'    => $fy->BASTAR,                    
                    'BITTAR'    => date('Y-m-d'),
                    'FIYATGRUBU'=> $fy->FIYATGRUBU,
                    'created_at'=> date('Y-m-d H:i:s')
                    ]);
            }
            return view('admin.fiyatliste.Netsisguncelle');
       
    }

    public function NetsisKurguncelle()
    {
        set_time_limit(0);
        if(Auth::user())
        {
            $maxKur = Doviz::orderby('TARIH','desc')->first();
            if($maxKur)
            {
                $kurlar = DB::connection('LS')->select('select * from NETSIS..DOVIZ where TARIH>?',array($maxKur->TARIH));
            }
            else
            {
                $kurlar = DB::connection('LS')->select('select * from NETSIS..DOVIZ where TARIH>?',array('2016-01-01'));
            }
            foreach($kurlar as $kur)
            {
                DB::table('kurlar')->insert([
                    'TARIH' => $kur->TARIH,
                    'SIRA' => $kur->SIRA,
                    'DOV_ALIS' => $kur->DOV_ALIS,
                    'DOV_SATIS' => $kur->DOV_SATIS,
                    'EFF_ALIS' => $kur->EFF_ALIS,
                    'EFF_SATIS' => $kur->EFF_SATIS
                    ]);
            }

            KurTipleri::truncate();
            $tipler = DB::connection('LS')->select('select * from NETSIS..KUR');
            foreach($tipler as $tp)
            {
                DB::table('kurtipleri')->insert([
                    'SIRA' => $tp->SIRA,
                    'BIRIM'=> $tp->BIRIM,
                    'ISIM' => $tp->ISIM,
                    'NETSISSIRA' => $tp->NETSISSIRA
                    ]);
            }

            return view('admin.fiyatliste.Netsisguncelle');
        }
        else
        {
            return view('errors.503');
        }
    }


    public function createUser()
    {
        $firma = explode(';',$_POST['carikod']);
        DB::table('users')->insert([
            'email' => $_POST['email'], 
            'name' => $_POST['name'], 
            'yetki' => $_POST['yetki'], 
            'carikod' => $firma[0], 
            'cari_id' => $firma[1], 
            'password' => bcrypt($_POST['password'])
            ]); 
        $users = User::get();
        $tblcari   = DB::connection('LS')->select('SELECT CARI_KOD,CARI_ISIM  FROM [TBLCASABIT]');

    

        $data = ['ad' => $_POST['name'] , 'username' => $_POST['email'], 'pass' => $_POST['password'] ];
        Mail::send('emails.welcome', $data, function($message) use($data)
        {
            $message->to($_POST['email'], $data['ad'])
            ->subject('Egebant B2B - Hoşgeldiniz')
            
            ->replyTo('egebant@egebant.com.tr', 'Egebant');
        });
        return redirect('/admin/users');
    }

    public function aktifyap(Request $request)
    {
        DB::table('users')->where('id',$_POST['user_id'])->update(['deleted_at' => NULL]);
        flash()->error('Kullanıcı hesabı Aktif hale getirilmiştir.');
        return redirect('/admin/users');
    }

    public function pasifyap(Request $request)
    {
        DB::table('users')->where('id',$_POST['user_id'])->update(['deleted_at' => date('Y-m-d H:i:s')]);
        flash()->error('Kullanıcı hesabı pasif hale getirilmiştir.');
        return redirect('/admin/users');
    }

    public function test()
    {
        return tblcari();
    }

    public function testmail()
    {
        $data = ['ad' => 'Tuana Şeyma', 'soyad' => 'Eldem' , 'mesaj' => 'mesaj buraya gelecek değişken'];

        Mail::send('emails.test', $data, function($message) use($data)
        {
            $message->to('rasit@dqturkiye.com', $data['ad'])
            ->subject('test mail B2B!')

            ->replyTo('rasitulcay@gmail.com', 'Rasit Gmail');
        });
    }
}
