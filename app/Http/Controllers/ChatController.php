<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use DB;
use App\Chat;
use App\User;
use App\Company;


class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chatlog = Chat::with('user','mt')->where('mt_id',Auth::user()->id)->orderby('id','desc')->groupBy('user_id')->simplePaginate(15);
        //$chatlog = Chat::with('user','mt','firma')->where('mt_id',Auth::user()->id)->orderby('id','desc')->simplePaginate(15);
        return view('mt.mailbox.index',compact('chatlog'));
    }

    public function mesajgor($id)
    {   
        $chatlog = Chat::with('user')->where('mt_id',Auth::user()->id)->where('user_id',$id)->orderby('id','desc')->simplePaginate(15);
        $kim = Chat::with('user')->where('mt_id',Auth::user()->id)->where('user_id',$id)->first();
        $okundu = DB::table('chatlog')
            ->where('user_id',$id)
            ->where('from_id',$kim->user->id)
            ->update(['okundu' => 1]);
        return view('mt.mailbox.mesajgor',compact('chatlog','kim'));
    }


    public function mesajgonder()
    {
        DB::table('chatlog')->insert([
            'user_id' => Auth::user()->id, 
            'mt_id' => $_POST['mt_id'], 
            'from_id' => $_POST['from_id'], 
            'mesaj' => $_POST['mesaj'], 
            'created_at' => date('Y-m-d H:i:s')
            ]);
        $mesajlar = Chat::with('user','mt')->where('user_id',Auth::user()->id)->orderby('id','desc')->simplePaginate(5);

        return redirect('/');
    }

    public function MTmesajgonder()
    {
        DB::table('chatlog')->insert([
            'user_id'   => $_POST['user_id'], 
            'mt_id'     => $_POST['mt_id'], 
            'from_id'   => $_POST['from_id'], 
            'mesaj'     => $_POST['mesaj'], 
            'created_at' => date('Y-m-d H:i:s')
            ]);
        $mesajlar = Chat::with('user','mt')->where('user_id',Auth::user()->id)->orderby('id','desc')->simplePaginate(5);

        return redirect('/mt/mesajlar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
