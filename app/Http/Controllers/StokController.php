<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Cart;
use Auth;
use App\Company;
use App\Http\Requests;
use App\Http\Redirect;
use App\User;
use App\Stok;
use App\Photo;
use App\Segment;
use DB;

class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tblstok = Stok::get();
        return view('admin.stok',compact('tblstok'));
    }


    public function indexUser()
    {
        $user   = User::find(Auth::user()->id);
        $company= Company::find($user->cari_id);
        $firmakod = $company->code; 
        $stoktip = $company->stoktip;
        $tblstok= Stok::with('defaultfoto')->get();
        $sepet  = Cart::content();
        return view('user.stok.index',compact('tblstok','sepet','firmakod','stoktip'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stok = DB::connection('LS')->select('select SUBE_KODU,ISLETME_KODU,STOK_KODU,URETICI_KODU,STOK_ADI,GRUP_KODU,KOD_1,KOD_2,KOD_3,KOD_4,KOD_5,OLCU_BR1,OLCU_BR2,KDV_ORANI,ASGARI_STOK from TBLSTSABIT where STOK_KODU=?',array($_POST['kod']))[0];
        $stokkod = DB::connection('LS')->select('select STOK_KODU,CARI_KOD,CARISTOK_KODU from TBLCARISTOK WHERE STOK_KODU=? and CARI_KOD is not NULL',array($_POST['kod']));
        $stkmik = DB::connection('LS')->select('select TOP_GIRIS_MIK-TOP_CIKIS_MIK as MIKTAR from TBLSTOKPH where DEPO_KODU=? and STOK_KODU=?',array('51',$_POST['kod']));
        $top = 0;
        foreach($stkmik as $stk)
        {
            $top = $top + $stk->MIKTAR;
        }
        return view('admin.stok.edit',compact('stok','stokkod','top'));
    }

    public function createList()
    {
        $stok = DB::connection('LS')->select('select SUBE_KODU, ISLETME_KODU, STOK_KODU, URETICI_KODU, STOK_ADI, GRUP_KODU, KOD_1, KOD_2, KOD_3, KOD_4, KOD_5, OLCU_BR1, OLCU_BR2 from TBLSTSABIT where DEPO_KODU=?',array('60'));
        return view('admin.stok.NetsisStokListe',compact('stok'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $stokkod = DB::connection('LS')->select('select STOK_KODU,CARI_KOD,CARISTOK_KODU from TBLCARISTOK WHERE STOK_KODU=? and CARI_KOD is not NULL',array($_POST['stokkod']));
        $custom = '';
        if($stokkod)
        {
            foreach($stokkod as $stkodu)
            {
                $custom .= $stkodu->CARI_KOD.'|'.$stkodu->CARISTOK_KODU.';';
            }
        }
        else
        {
            $custom = '0';
        }

        DB::table('stoks')->insert([
            'name' => $_POST['stokad'],
            'code' => $_POST['stokkod'],
            'SUBE_KODU' => $_POST['subekod'],
            'URETICI_KODU' => $_POST['uretici'],
            'GRUP_KODU' => $_POST['grup'],
            'KOD_1' => $_POST['kod1'],
            'KOD_2' => $_POST['kod2'],
            'KOD_3' => $_POST['kod3'],
            'KOD_4' => $_POST['kod4'],
            'KOD_5' => $_POST['kod5'],
            'OLCU_BR1' => $_POST['olcu1'],
            'OLCU_BR2' => $_POST['olcu2'],
            'aciklama' => $_POST['editor1'],
            'miktar'   => $_POST['miktar'],
            'kdv'      => $_POST['kdv'],
            'dipmiktar'=> $_POST['asgari'],
            'custom'   => $custom, 
            'created_at' => date('Y-m-d H:i:s')
            ]); 
        return redirect('/admin/stok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function detay($id)
    {
        $stok = Stok::find($id);
        $photo = Photo::where('stok_id', $id)->first();;
        return view('user.stok.detay',compact('stok','photo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function photostore($id)
    {

            
            $imageType = $_FILES['exampleInputFile']['type'];
            $data = file_get_contents($_FILES['exampleInputFile']['tmp_name']);
            $base64 = 'data:' . $imageType . ';base64,' . base64_encode($data);
            DB::table('photo')->insert([
            'stok_id' => $id,
            'foto' => $base64,
            'created_at' => date('Y-m-d H:i:s')
            ]);
            return redirect('/admin/stok/edit/'.$id);
      
        
    }

    public function edit($id)
    {
        if(isset($_POST['sil']))
        {
            DB::table('photo')->delete($_POST['photo_id']);
            return redirect('/admin/stok/edit/'.$id);
        }
        if(isset($_POST['default']))
        {
            DB::table('stoks')->where('id', $id)->update([
            'foto_id' => $_POST['photo_id'], 
            'updated_at' => date('Y-m-d H:i:s')
            ]);
            return redirect('/admin/stok/edit/'.$id);
        }
        $stok = Stok::find($id);
        $photo = Photo::where('stok_id', $id)->get();;
        $stokkod = DB::connection('LS')->select('select STOK_KODU,CARI_KOD,CARISTOK_KODU from TBLCARISTOK WHERE STOK_KODU=? and CARI_KOD is not NULL',array($stok->code));
        return view('admin.stok.photo',compact('stok','stokkod','photo'));
    }

    public function update(Request $request)
    {
        DB::table('stoks')->where('id',$_POST['stok_id'])->update([
            'aciklama' => $_POST['editor1'],
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect('/admin/stok/edit/'.$_POST['stok_id']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
