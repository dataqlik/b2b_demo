<?php
use \App\Handlers\Commands;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::group(['middleware'=>\App\Http\Middleware\AdminMiddleware::class], function(){
/* Admin Tarafı */
Route::get('/admin', 'HomeController@index');

/* Kullanıcı Tanımları */
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/users/edit/{id}', 'AdminController@editUser');
Route::post('/admin/users/create','AdminController@createUser');
Route::post('/admin/users/update','AdminController@updateUser');
Route::post('/admin/users/delete','AdminController@deleteUser');
Route::post('/admin/user/aktif', 'AdminController@aktifyap');
Route::post('/admin/user/pasif', 'AdminController@pasifyap');

/*Segment Tanımlamaları*/
Route::get('/admin/segment', 'SegmentController@index');
Route::post('/admin/segment/create', 'SegmentController@create');


/* Stok Tanımlamaları */
Route::get('/admin/stok', 'StokController@index');
Route::post('/admin/stok/create', 'StokController@create');
Route::get('/admin/stok/edit/{key}', 'StokController@edit');
Route::post('/admin/stok/edit/{key}', 'StokController@edit');
Route::post('/admin/stok/photo/{key}', 'StokController@photostore');
Route::post('/admin/stok/save', 'StokController@store');
Route::post('/admin/stok/update', 'StokController@update');
/* Netsisten toplu stok çekme*/
Route::get('/admin/stok/NetsisStokListe', 'StokController@createList');

/*Kampanya Tanımlamaları*/
Route::get('/admin/kampanya', 'KampanyaController@index');
Route::post('/admin/kampanya/create','KampanyaController@create');


/* Admin Onaylanmamış Siparişler */
Route::get('/admin/siparis', 'SiparisController@adminindex');
Route::get('/admin/siparis/detay/{key}', 'SiparisController@AdminListindex');



Route::get('/admin/fiyatliste/guncelle','AdminController@Netsisfiyatguncelle');
Route::get('/admin/kur/guncelle','AdminController@NetsisKurguncelle');

/* Firma Tanımlamaları */
Route::get('/admin/company', 'CompanyController@index');
Route::post('/admin/company/create', 'CompanyController@create');
Route::post('/admin/company/save', 'CompanyController@store');
Route::post('/admin/company/update', 'CompanyController@update');
Route::get('/admin/company/edit/{key}', 'CompanyController@edit');

/* Logviewer */
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
/** Admin tarafı bitişi **/

});


/************************************************************************************************/

/** MT Tarafı **/
Route::group(['middleware'=>\App\Http\Middleware\MTMiddleware::class], function(){
Route::post('/mt/mesajgonder','ChatController@MTmesajgonder');

Route::get('/mt/firmalar', 'CompanyController@mtindex');
Route::get('/mt/siparisler', 'SiparisController@mtindex');
Route::get('/mt/siparis/detay/{key}', 'SiparisController@MtListindex');
Route::get('/mt/siparis/islem/{key}', 'SiparisController@Mtsiparis');
Route::get('/mt/siparis/islem/basla/{key}', 'SiparisController@MtsiparisBasla');

Route::get('/mt/siparis/islem/iptal/{key}/{key1}', 'SiparisController@MtSiparisKalemIptal');
Route::get('/mt/siparis/islem/onay/{key}/{key1}', 'SiparisController@MtSiparisKalemOnay');
Route::get('/mt/siparis/islem/temin/{key}/{key1}', 'SiparisController@MtSiparisKalemTemin');
Route::post('/mt/siparis/islem/tamam','SiparisController@erpaktar');
Route::get('/mt/mesajlar', 'ChatController@index');
Route::get('/mt/mesajlar/{key}', 'ChatController@mesajgor');
Route::post('/mt/mesajgonder','ChatController@MTmesajgonder');
});

/************************************************************************************************/

/** Bayi Tarafı **/
Route::group(['middleware'=>\App\Http\Middleware\UserMiddleware::class], function(){
Route::get('/siparis', 'SiparisController@index');
Route::get('/siparis/detay/{key}', 'SiparisController@Listindex');


Route::get('/urunler', 'StokController@indexUser');
Route::get('/kampanyalar', 'KampanyaController@index');
Route::get('/raporlar', 'RaporController@index');
Route::get('/fiyat-liste', 'FiyatlistesiController@index');

/* Ödeme İşlemleri */
Route::get('/odeme', 'OdemeController@index');

/* Taslak Sipariş */
Route::get('/taslaklar', 'SiparisController@taslakindex');
Route::get('/taslak/detay/{key}', 'SiparisController@taslakdetay');
Route::post('/taslak/siparis', 'SiparisController@taslaktosiparis');


/* Sipariş İşlemleri */
Route::post('/urunler/ekle','SiparisController@sepeteekle');
Route::get('/urunler/sepet/sil/{key}','SiparisController@destroy');
Route::post('/urunler/sepet/adetguncelle/{key}','SiparisController@adetguncelle');
Route::get('/urunler/sepet/bosalt','SiparisController@sepetdestroy');
Route::get('/urunler/sepet/siparis','SiparisController@create');
Route::post('/urunler/sepet/taslak','SiparisController@taslak');
Route::get('/urunler/detay/{key}','StokController@detay');

Route::get('/odemeler', 'OdemeController@index');
});


/** Bayi tarafı Bitişi**/

Route::get('/ayarlar', 'AyarlarController@index');
Route::post('/ayarlar/kaydet', 'AyarlarController@update');
Route::auth();
Route::get('/home', 'HomeController@index');
Route::post('/chat/mesajgonder','ChatController@mesajgonder');


/* Taslak */

Route::get('/fiyatliste/guncelle','AdminController@Netsisfiyatguncelle');
Route::get('/kur/guncelle','AdminController@NetsisKurguncelle');
Route::get('/mail/check','TaskController@mailindex');
Route::get('/stok/guncelle','TaskController@NetsisStokMiktar');

/* Task bitişi */

Route::group(['middleware'=>\App\Http\Middleware\AdminMiddleware::class], function(){

Route::get('/test','AdminController@test');

Route::get('/testmail','AdminController@testmail');


Route::get('/try',function(){
    $dataArray = PDO::getAvailableDrivers();
    dd($dataArray);
});




});

Route::get('/kur','KurController@Kurindex');

Route::get('/info',function(){
    return route('my_route_name');
});

Route::get('/trysql',function(){
    $data = DB::connection('LS')->insert('INSERT INTO EGE2016..URTKULLANICI_1 (KULLANICI_KODU,SIFRESI,OPERASYON_KODU,GRUP_KODU,ADI_SOYADI,ADMIN) VALUES (?,?,?,?,?,?)',array('1','1','1','1','1','1'));
});