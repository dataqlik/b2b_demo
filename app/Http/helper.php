<?php

function ipbul()
{
	if(getenv("HTTP_CLIENT_IP")) { 
		$ip = getenv("HTTP_CLIENT_IP"); 
	} elseif(getenv("HTTP_X_FORWARDED_FOR")) { 
		$ip = getenv("HTTP_X_FORWARDED_FOR"); 
		if (strstr($ip, ',')) { 
			$tmp = explode (',', $ip); 
			$ip = trim($tmp[0]); 
		} 
	} else { 
		$ip = getenv("REMOTE_ADDR"); 
	} 
	return $ip;
}

function tblcariad($id)
{
	$data = DB::connection('LS')->select('SELECT CARI_ISIM  FROM TBLCASABIT where CARI_KOD=?',array($id))[0]; 
	return $data->CARI_ISIM;
}

function mtad($id)
{
	$cari = DB::connection('mysql')->select('SELECT mt FROM companies where carikod=?',array($id));
	if(count($cari)==1)
	{
		foreach($cari as $cr)
		{
			$mtemsil = DB::connection('mysql')->select('select name from users where id=?',array($cr->mt));
			foreach($mtemsil as $mt)
			{
				return $mt->name;
				break;
			}
		}
	}
	else
	{
		return "";
	}
}

function tarihyaz($tar) {
    $tar = substr($tar,0,10);
    $yil = substr($tar,6,4);
    $ay = substr($tar,3,2);
    $gun = substr($tar,0,2);
    $new_tar = $yil.'-'.$ay.'-'.$gun;
    return $new_tar;
}


function tarihoku($tar) {
    $tar = substr($tar,0,10);
    $yil = substr($tar,0,4);
    $ay = substr($tar,5,2);
    $gun = substr($tar,8,2);
    $new_tar = $gun."-".$ay."-".$yil;
    return $new_tar;
}

function saatoku($tar) {
    $tar = substr($tar,10,10);
    $yil = substr($tar,0,4);
    $ay = substr($tar,5,2);
    $gun = substr($tar,8,2);
    $new_tar = $gun."-".$ay."-".$yil;
    return $tar;
}

function yetki($id)
{
	switch($id)
	{
		case '1' : $a = 'Superuser'; break; 
		case '2' : $a =  'Bayi'; break;
		case '3' : $a =  'Müşteri Temsilcisi'; break; 
		default  : $a =  'User Error..!';
	}
	return $a;
}

function paracevir($text)
{
	$tip = explode('.',$text);
	return $tip[0].','.$tip[1];
}

function karakter($text)
{
	$find       = array('Þ','Ý');
	$replace    = array('Ş','İ');
	$newtext = str_ireplace($find, $replace, $text);
	return $newtext;
}


?>
