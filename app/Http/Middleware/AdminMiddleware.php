<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user())
        {
            if ($request->user()->yetki == '1')
            { 
              return $next($request);       
            }
            else
            {
                return redirect('/');
            }   
        }
        else
        {
            return redirect('/');
        }
    }
}
