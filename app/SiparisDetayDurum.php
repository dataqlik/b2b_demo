<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiparisDetayDurum extends Model
{
    protected $table = 'siparisdetay_durum';
}
