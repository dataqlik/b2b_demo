<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SiparisDetay extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'siparisdetay';


    public function urun()
    {
    	return $this->hasOne('App\Stok','code','stokkodu');
    }
    public function sipdetay()
    {
        return $this->hasOne('App\SiparisDetayDurum','durum_id','durum');
    }
}
