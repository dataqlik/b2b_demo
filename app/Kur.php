<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kur extends Model
{
    protected $table = 'kurlar';

    public function tip()
    {
        return $this->hasOne('App\KurTipleri', 'SIRA', 'SIRA');
    }
}
