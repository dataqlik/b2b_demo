<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'companies';
    public function mt()
    {
        return $this->hasOne('App\User', 'id', 'mt_id');
    }
    public function segment()
    {
        return $this->hasOne('App\Segment', 'id', 'segment_id');
    }
}
