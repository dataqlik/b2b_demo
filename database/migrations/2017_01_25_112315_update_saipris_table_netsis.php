<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSaiprisTableNetsis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siparis', function (Blueprint $table) {
            $table->string('depokodu')->default('0');
            $table->string('erpkodu')->default('B2B000000000000');
            $table->date('fiyat_tarihi')->nullable();
            $table->string('prefix')->default('B2B');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
