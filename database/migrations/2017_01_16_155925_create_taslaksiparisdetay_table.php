<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaslaksiparisdetayTable extends Migration
{
    public function up()
    {
        Schema::create('taslaksiparisdetay', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('sip_id');
            $table->string('stokkodu');
            $table->string('birim');
            $table->integer('adet')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taslaksiparisdetay');
    }
}
