<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatlog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('mt_id');
            $table->integer('from_id');
            $table->text('mesaj');
            $table->integer('okundu')->default('0');
            $table->datetime('okunmazaman');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chatlog');
    }
}
