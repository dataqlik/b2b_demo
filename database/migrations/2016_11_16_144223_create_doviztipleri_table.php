<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoviztipleriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurtipleri', function (Blueprint $table) {
            $table->integer('SIRA');
            $table->integer('BIRIM');
            $table->string('ISIM');
            $table->string('NETSISSIRA');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kurtipleri');
    }
}
