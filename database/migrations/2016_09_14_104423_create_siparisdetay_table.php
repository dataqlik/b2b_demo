<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiparisdetayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siparisdetay', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('siparis_id');
            $table->string('stokkodu');
            $table->string('birim');
            $table->integer('adet')->default('0');
            $table->decimal('fiyat',5,2);
            $table->decimal('tutar',5,2);
            $table->decimal('indirimoran',2,2);
            $table->decimal('indirimtutar',5,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siparisdetay');
    }
}