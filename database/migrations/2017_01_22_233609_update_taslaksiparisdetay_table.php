<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaslaksiparisdetayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taslaksiparisdetay', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('tsip_id');
            $table->string('stokkodu');
            $table->string('birim');
            $table->integer('adet')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taslaksiparisdetay');
    }
}
