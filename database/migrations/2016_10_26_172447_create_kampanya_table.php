<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKampanyaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kampanya', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kampanya_ad');
            $table->integer('segment_id');
            $table->text('aciklama');
            $table->date('basla');
            $table->date('bitir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kampanya');
    }
}
