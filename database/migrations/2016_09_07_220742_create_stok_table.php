<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stoks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('b2b_name');
            $table->text('name');
            $table->string('SUBE_KODU');
            $table->string('ISLETME_KODU');
            $table->string('URETICI_KODU');
            $table->string('GRUP_KODU');
            $table->string('KOD_1');
            $table->string('KOD_2');
            $table->string('KOD_3');
            $table->string('KOD_4');
            $table->string('KOD_5');
            $table->string('OLCU_BR1');
            $table->string('OLCU_BR2');
            $table->text('aciklama');
            $table->integer('foto_id')->default(0);
            $table->integer('durum')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stoks');
    }
}
