<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiparisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siparis', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('cari_id');
            $table->bigInteger('user_id');
            $table->string('carikod');
            $table->datetime('siparis_tarih');
            $table->decimal('tutar',8,2);
            $table->string('durum')->default('0');
            $table->timestamps();
        });
        // dip toplam iskonto oran ve tutarlarını da tutalım.
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siparis');
    }
}
