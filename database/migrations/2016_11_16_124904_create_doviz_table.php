<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDovizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurlar', function (Blueprint $table) {
            $table->date('TARIH');
            $table->integer('SIRA');
            $table->decimal('DOV_ALIS',18,9);
            $table->decimal('DOV_SATIS',18,9);
            $table->decimal('EFF_ALIS',18,9);
            $table->decimal('EFF_SATIS',18,9);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kurlar');
    }
}
