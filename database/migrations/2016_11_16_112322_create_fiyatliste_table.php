<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiyatlisteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('fiyatliste', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('INCKEYNO');
            $table->string('STOKKODU');
            $table->string('A_S');
            $table->decimal('FIYAT1',5,2);
            $table->decimal('FIYAT2',5,2);
            $table->decimal('FIYAT3',5,2);
            $table->decimal('FIYAT4',5,2);
            $table->integer('FIYATDOVIZTIPI');
            $table->datetime('BASTAR')->nullable();
            $table->datetime('BITTAR')->nullable();
            $table->string('FIYATGRUBU');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fiyatliste');
    }
}
