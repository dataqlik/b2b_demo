<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaslaksiparisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taslaksiparis', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('cari_id');
            $table->bigInteger('user_id');
            $table->string('carikod');
            $table->datetime('taslak_tarih');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taslaksiparis');
    }
}
